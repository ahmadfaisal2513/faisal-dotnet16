﻿using System.ComponentModel.DataAnnotations;

namespace VirtualAccountWebMvc.ViewModels.BankAccount
{
    public class DeleteBankAccountViewModel : BaseViewModel
    {
        [Display(Name = "Bank Account ID")]
        [Required(ErrorMessage = "Bank Account ID perlu di isi")]
        public long BankAccountId { get; set; }
    }
}
