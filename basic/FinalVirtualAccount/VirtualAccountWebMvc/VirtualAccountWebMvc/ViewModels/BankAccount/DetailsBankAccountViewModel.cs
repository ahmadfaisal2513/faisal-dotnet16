﻿using System.ComponentModel.DataAnnotations;

namespace VirtualAccountWebMvc.ViewModels.BankAccount
{
    public class DetailsBankAccountViewModel : BaseViewModel
    {
        [Display(Name = "Bank Account ID")]
        [Required(ErrorMessage = "bank account id harus di isi")]
        public long BankAccountID { get; set; }

        [Display(Name = "Virtual Account ID")]
        public long VirtualAccountID { get; set; }

        [Display(Name = "Bank ID")]
        public long BankID { get; set; }

        [Display(Name = "Bank Account Number")]
        public long BankAccountNumber { get; set; }

        [Display(Name = "Bank Account Owner")]
        public string BankAccountOwner { get; set; }

        [Display(Name = "Balance")]
        public decimal Balance { get; set; }
    }
}
