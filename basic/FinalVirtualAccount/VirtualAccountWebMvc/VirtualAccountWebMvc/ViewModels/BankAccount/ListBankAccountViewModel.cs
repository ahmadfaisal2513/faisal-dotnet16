﻿using VirtualAccountWebMvc.ViewModels.BankAccount;

namespace VirtualAccountWebMvc.ViewModels.BankAccount
{
    public class ListBankAccountViewModel : BaseViewModel
    {
        public List<DetailsBankAccountViewModel> BankAccounts { get; set; } = new List<DetailsBankAccountViewModel> ();
    }
}
