﻿using System.ComponentModel.DataAnnotations;



namespace VirtualAccountWebMvc.ViewModels.BankAccount
{
    public class CreateBankAccountViewModel : BaseViewModel
    {
        [Display(Name = "Bank Account ID")]
        [Required(ErrorMessage = "bank account id harus di isi")]
        public long BankAccountID { get; set; }

        [Display(Name = "Virtual Account ID")]
        [Required(ErrorMessage = "virtual account id harus di isi")]
        public long VirtualAccountID { get; set; }

        [Display(Name = "Bank ID")]
        [Required(ErrorMessage = "bank id harus di isi")]
        public long BankID { get; set; }

        [Display(Name = "Bank Account Number")]
        [Required(ErrorMessage = "bank account name harus di isi")]
        public long BankAccountNumber { get; set; }

        [Display(Name = "Bank Account Owner")]
        [Required(ErrorMessage = "bank account owner harus di isi")]
        public string BankAccountOwner { get; set; }

        [Display(Name = "Balance")]
        [Required(ErrorMessage = "balance harus lebih dari 0")]
        public decimal Balance { get; set; }
    }
}
