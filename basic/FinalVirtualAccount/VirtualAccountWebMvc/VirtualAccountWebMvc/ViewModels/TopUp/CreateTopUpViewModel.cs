﻿using System.ComponentModel.DataAnnotations;

namespace VirtualAccountWebMvc.ViewModels.TopUp
{
    public class CreateTopUpViewModel : BaseViewModel
    {
        [Display(Name = "TopUp ID")]
        [Required(ErrorMessage = "Top id tidak boleh kosong")]
        public long TopUpID { get; set; }

        [Display(Name = "Virtual Account ID")]
        [Required(ErrorMessage = "Virtual Account ID tidak boleh kosong")]
        public long VirtualAccountID { get; set; }

        [Display(Name = "Topup Amount")]
        [Required(ErrorMessage = "Topup Amount harus lebih dari 0")]
        public decimal TopUpAmount { get; set; }

        [Display(Name = "TopUpDate")]
        [Required(ErrorMessage = "Tanggal tidak boleh kosong")]
        public DateTime TopUpDate { get; set; }

        [Display(Name = "Bank ID")]
        [Required(ErrorMessage = "Bank ID tidak boleh kosong")]
        public long BankID { get; set; }

        [Display(Name ="Bank Name")]
        [Required(ErrorMessage = "Nama Bank tidak boleh kosong")]
        public string BankName { get; set; }

        [Display(Name = "Bank Account Number")]
        [Required(ErrorMessage = "Bank Account Number harus di isi")]
        public long BankAccountNumber { get; set; }

        [Display(Name = "Bank Account Owner")]
        [Required(ErrorMessage = "Nama pemilik bank harus di isi")]
        public string BankAccountOwner { get; set; }
    }
}
