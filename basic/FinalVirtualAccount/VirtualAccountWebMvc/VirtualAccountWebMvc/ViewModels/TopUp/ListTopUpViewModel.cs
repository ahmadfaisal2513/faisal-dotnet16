﻿namespace VirtualAccountWebMvc.ViewModels.TopUp
{
    public class ListTopUpViewModel : BaseViewModel
    {
        public List<DetailsTopUpViewModel> TopUps { get; set; } = new List<DetailsTopUpViewModel>();
    }
}
