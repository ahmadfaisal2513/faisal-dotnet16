﻿using System.ComponentModel.DataAnnotations;


namespace VirtualAccountWebMvc.ViewModels.TopUp
{
    public class DetailsTopUpViewModel : BaseViewModel
    {
        [Display(Name = "Topup ID")]
        [Required(ErrorMessage = "Topup ID harus tidak boleh kosong")]
        public long TopUpID { get; set; }

        [Display(Name = "Virtual Account ID")]
        public long VirtualAccountID { get; set; }

        [Display(Name = "Topup Amount")]
        public decimal TopUpAmount { get; set; }
        
        [Display(Name = "Topup Date")]
        public DateTime TopUpDate { get; set; }

        [Display(Name = "Bank ID")]
        public long BankID { get; set; }

        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [Display(Name = "Bank Account Number")]
        public long BankAccountNumber { get; set; }

        [Display(Name = "Bank Account Owner")]
        public string BankAccountOwner { get; set; }

    }
}
