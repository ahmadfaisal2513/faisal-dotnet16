﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VirtualAccountWebMvc.ViewModels;

public class DeleteUserViewModel : BaseViewModel
{
    [Key]
    [Display(Name = "Email")]
    [Required(ErrorMessage = "Invalid Email")]
    [DataType(DataType.EmailAddress)]
    public string Email { get; set; } = string.Empty;

}