﻿using VirtualAccountWebMvc.ViewModels;

namespace VirtualAccountWebMvc.ViewModels;

public class ListUserViewModel : BaseViewModel
{
    public List<DetailsUserViewModel> Users { get; set; } = new List<DetailsUserViewModel>();
}