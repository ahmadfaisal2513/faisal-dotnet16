﻿using System.Web;
using VirtualAccountServices.Models;
using Microsoft.AspNetCore.Mvc;

namespace VirtualAccountWebMvc.Controllers
{
    public class BaseController : Controller
    {

        protected VirtualAccountDbContext _db;
        protected string _loginID = string.Empty;

        public BaseController(VirtualAccountDbContext db)
        {
            _db = db;

        }

        protected void _setLoginID()
        {
            var id = HttpContext.Items["LoginID"];
            if (id != null)
            {
                _loginID = id.ToString();
            }
        }

        protected string _decodeUrl(string p)
        {
            return HttpUtility.UrlDecode(p);
        }
    }
}
