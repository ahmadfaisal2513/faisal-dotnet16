﻿using Microsoft.AspNetCore.Mvc;
using VirtualAccountServices.Models;
using VirtualAccountWebMvc.ViewModels.TopUp;
using VirtualAccountServices.Services;
using VirtualAccountServices;

namespace VirtualAccountWebMvc.Controllers
{
    public class TopUpController : BaseController
    {
        public TopUpController(VirtualAccountDbContext db) : base(db)
        {
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Create(CreateTopUpViewModel pCreateTopUpVM)
        {
            ViewBag.FormMode = "Create";
            return View("TopUp", pCreateTopUpVM);
        }

        [HttpPost]
        public IActionResult Insert(CreateTopUpViewModel pCreateTopupVM)
        {
            try
            {
                ViewBag.FormMode = "Create";
                if (ModelState.IsValid)
                {
                    _setLoginID();
                    TopUpService _topupService = new TopUpService(_db, _loginID);
                    _topupService.AddTopup(/*pCreateTopupVM.TopUpID,*/
                        pCreateTopupVM.VirtualAccountID,
                        pCreateTopupVM.TopUpAmount,
                        /*pCreateTopupVM.TopUpDate,*/
                        pCreateTopupVM.BankID,
                        pCreateTopupVM.BankName,
                        pCreateTopupVM.BankAccountNumber,
                        pCreateTopupVM.BankAccountOwner
                        );

                    var _listTopupVM = new ListTopUpViewModel()
                    {
                        Message = "Data sudah tersimpan",
                        t = pCreateTopupVM.t,
                    };
                    return RedirectToAction("List", _listTopupVM);

                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            return View("Edit", pCreateTopupVM);
        }

        /*[HttpPost]
        public IActionResult Update(CreateTopUpViewModel pEditTopupVM)
        {
            try
            {
                ViewBag.FormMode = "Edit";
                if (ModelState.IsValid)
                {
                    _setLoginID();
                    TopUpService _topupService = new TopUpService(_db, _loginID);
                    _topupService.UpdateTopup(pEditTopupVM.TopUpID,
                        pEditTopupVM.VirtualAccountID,
                        pEditTopupVM.TopUpAmount,
                        pEditTopupVM.BankID,
                        pEditTopupVM.BankName,
                        pEditTopupVM.BankAccountNumber,
                        pEditTopupVM.BankAccountOwner);

                    var _listTopupVM = new ListTopUpViewModel()
                    {
                        Message = "Data sudah diubah",
                        t = pEditTopupVM.t,
                    };
                    return RedirectToAction("List", _listTopupVM);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            return View("Edit", pEditTopupVM);
        }*/
        public IActionResult List(ListTopUpViewModel pListTopupVM)
        {
            try
            {
                _setLoginID();
                TopUpService _topupService = new TopUpService(_db, _loginID);
                var _topups = _topupService.GetTopups();

                var _listTopupVM = new ListTopUpViewModel();
                _listTopupVM.UpdateTokenAndMessage(pListTopupVM.t, pListTopupVM.Message);

                if (_topups != null && _topups.Count > 0)
                {
                    foreach (var _topup in _topups)
                    {
                        _listTopupVM.TopUps.Add(
                            new DetailsTopUpViewModel()
                            {
                                TopUpID = _topup.TopUpID,
                                VirtualAccountID = _topup.VirtualAccountID,
                                TopUpAmount = _topup.TopUpAmount,
                                TopUpDate = _topup.TopUpDate,
                                BankID = _topup.BankID,
                                BankName = _topup.BankName,
                                BankAccountNumber = _topup.BankAccountNumber,
                                BankAccountOwner = _topup.BankAccountOwner,
                            }
                        );
                    }
                }

                return View("List", _listTopupVM);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            return View("List", pListTopupVM);
        }
    }
}
