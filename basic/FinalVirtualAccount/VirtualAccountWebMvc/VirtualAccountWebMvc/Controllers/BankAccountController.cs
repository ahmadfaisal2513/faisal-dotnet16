﻿using Microsoft.AspNetCore.Mvc;
using VirtualAccountServices.Models;
using VirtualAccountWebMvc.ViewModels.BankAccount;
using VirtualAccountServices.Services;
using VirtualAccountServices;

namespace VirtualAccountWebMvc.Controllers
{
    public class BankAccountController : BaseController
    {
        public BankAccountController(VirtualAccountDbContext db) : base(db)
        {

        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create(CreateBankAccountViewModel pCreateBankAccountVM)
        {
            ViewBag.FormMode = "Create";
            return View("BankAccount", pCreateBankAccountVM);
        }

        [HttpPost]
        public IActionResult Insert(CreateBankAccountViewModel pCreateBankAccountVM)
        {
            try
            {
                ViewBag.FormMode = "Create";
                if (ModelState.IsValid)
                {
                    _setLoginID();
                    BankAccountService _bankAccountService = new BankAccountService(_db, _loginID);
                    _bankAccountService.AddBankAccount(pCreateBankAccountVM.BankAccountID,
                        pCreateBankAccountVM.BankID,
                        pCreateBankAccountVM.BankAccountNumber,
                        pCreateBankAccountVM.BankAccountOwner,
                        pCreateBankAccountVM.Balance);

                    var _listBankAccountVM = new ListBankAccountViewModel()
                    {
                        Message = "Data sudah Tersimpan",
                        t = pCreateBankAccountVM.t,
                    };

                    return RedirectToAction("list", _listBankAccountVM);

                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            return View("Edit", pCreateBankAccountVM);
        }

        public IActionResult List(ListBankAccountViewModel pListBankAccountVM)
        {
            try
            {
                _setLoginID();
                BankAccountService _bankAccountService = new BankAccountService(_db, _loginID);
                var _bankAccounts = _bankAccountService.GetBankAccounts();

                var _listBankAccountVM = new ListBankAccountViewModel();
                _listBankAccountVM.UpdateTokenAndMessage(pListBankAccountVM.t, pListBankAccountVM.Message);

                if (_bankAccounts != null && _bankAccounts.Count > 0)
                {
                    foreach (var _bankAccount in _bankAccounts)
                    {
                        _listBankAccountVM.BankAccounts.Add(
                            new DetailsBankAccountViewModel()
                            {
                            BankAccountID = _bankAccount.BankAccountID,
                            VirtualAccountID = _bankAccount.VirtualAccountID,
                            BankID = _bankAccount.BankID,
                            BankAccountNumber = _bankAccount.BankAccountNumber,
                            BankAccountOwner = _bankAccount.BankAccountOwner,
                            Balance = _bankAccount.Balance,
                            }
                        );
                    }
                }

                return View("List", _listBankAccountVM);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            return View("List", pListBankAccountVM);
        }
    }
       
}
