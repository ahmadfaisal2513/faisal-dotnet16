﻿using VirtualAccountServices.Models;
using VirtualAccountWebMvc.AppConfig;
using VirtualAccountWebMvc.ViewModels.Login;
using VirtualAccountWebMvc.ViewModels.Home;
using Microsoft.AspNetCore.Mvc;
using VirtualAccountServices;
using VirtualAccountServices.Services;
using VirtualAccountWebMvc.ViewModels;


namespace VirtualAccountWebMvc.Controllers
{
    public class HomeController : BaseController
    {

        private ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger,
            VirtualAccountDbContext db) : base(db)
        {
            _logger = logger;
            _db = db;
        }

/*        public IActionResult Index(ViewModels.Home.HomeViewModel homeViewModel)
        {
            return View(homeViewModel);
        }
*/
        public IActionResult Index()
        {
            var _loginViewModel = new LoginViewModel();
            if (DebugMode.Debug)
            {
                _loginViewModel.Email = "faisal@gmail.com";
                _loginViewModel.Password = "123456";
            }
            return View(_loginViewModel);
        }
        public IActionResult Main(BaseViewModel pBaseViewModel)
        {
            return View("Main", pBaseViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login([Bind("Email, Password")] LoginViewModel _loginVM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UserLoginService _userLoginService = new UserLoginService(_db, "");
                    var _login = _userLoginService.Login(_loginVM.Email,
                        _loginVM.Password);

                    var _homeViewModel = new HomeViewModel()
                    {
                        t = _login.LoginID
                    };
                    return RedirectToAction("Main", "Home", _homeViewModel);

                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return View("Index", _loginVM);
        }

        public IActionResult Logout(string t)
        {
            try
            {
                UserLoginService _userLoginService = new UserLoginService(_db, "");
                _userLoginService.Logout(t);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            return RedirectToAction("Index", "Login");

        }

    }
}