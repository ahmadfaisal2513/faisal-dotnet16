﻿using Microsoft.AspNetCore.Mvc;

namespace VirtualAccountWebMvc.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("Index", "Login");
        }
        public IActionResult Privacy()
        {
            return View();
        }
    }
}
