﻿using VirtualAccountServices.Models;
using VirtualAccountWebMvc.AppConfig;
using VirtualAccountWebMvc.ViewModels;
using Microsoft.AspNetCore.Mvc;
using VirtualAccountServices;
using VirtualAccountWebMvc.Controllers;

namespace VirtualAccountWebMvc.Areas.User.Controllers;

public class UserController : BaseController
{
    private object _listUserVM;

    public UserController(VirtualAccountDbContext db) : base(db)
    {
    }

    public IActionResult Edit(string id, EditUserViewModel pEditUserVM)
    {
        try
        {
            _setLoginID();
            id = _decodeUrl(id);
            UserService _userService = new UserService(_db, _loginID);
            var _user = _userService.FindByEmail(id, false);
            if (_user != null)
            {
                var _edituserVM = new EditUserViewModel()
                {
                    Email = _user.Email,
                    Username = _user.Username,
                    Active = _user.Active,
                    t = pEditUserVM.t
                };
                return View("Edit", _edituserVM);
            }
        }
        catch (Exception e)
        {
            ModelState.AddModelError("", e.Message);
        }
        return View("Edit", pEditUserVM);
    }

    public IActionResult Create(RegisterUserViewModel pRegisterUserVM)
    {
        if (DebugMode.Debug)
        {
            pRegisterUserVM.Email = "fathan@gmail.com";
            pRegisterUserVM.Username = "Fathan";
            pRegisterUserVM.Active = true;
            pRegisterUserVM.Password = "123456";
            pRegisterUserVM.ConfirmPassword = "123456";

        }
        return View("Create", pRegisterUserVM);
    }

    [HttpPost]
    public IActionResult Update(EditUserViewModel pEditUserVM)
    {
        try
        {
            if (ModelState.IsValid)
            {
                _setLoginID();
                UserService _userService = new UserService(_db, _loginID);
                _userService.UpdateUser(pEditUserVM.Email,
                    pEditUserVM.Username,
                    pEditUserVM.Active);

                var _listUserVM = new ListUserViewModel()
                {
                    Message = "Data sudah diubah",
                    t = pEditUserVM.t
                    ,
                };
                return RedirectToAction("List", _listUserVM);
            }
        }
        catch (Exception e)
        {
            ModelState.AddModelError("", e.Message);
        }

        return View("Edit", pEditUserVM);
    }


    [HttpPost]
    public IActionResult Insert(RegisterUserViewModel pRegisterUserVM)
    {
        try
        {
            if (ModelState.IsValid)
            {
                _setLoginID();
                UserService _userService = new UserService(_db, _loginID);
                _ = _userService.AddUser(pRegisterUserVM.Email,
                    pRegisterUserVM.Password,
                    pRegisterUserVM.ConfirmPassword,
                    pRegisterUserVM.Username,
                    pRegisterUserVM.Active);

                pRegisterUserVM.Message = "Data sudah tersimpan";

            }
        }
        catch (Exception e)
        {
            ModelState.AddModelError("", e.Message);
        }

        return View("Create", pRegisterUserVM);
    }


    [HttpPost]
    public IActionResult Delete(DeleteUserViewModel pDeleteUserVM)
    {
        ListUserViewModel _listUserVM = new ListUserViewModel();
        _listUserVM.t = pDeleteUserVM.t;

        try
        {
            if (ModelState.IsValid)
            {
                _setLoginID();
                UserService _userService = new UserService(_db, _loginID);
                _userService.DeleteUser(pDeleteUserVM.Email);

                _listUserVM.Message = "Data sudah dihapus";
                return RedirectToAction("List", _listUserVM);
            }
        }
        catch (Exception e)
        {
            ModelState.AddModelError("", e.Message);
        }

        return View("List", _listUserVM);

    }

    public IActionResult List(ListUserViewModel pListUserVM)
    {
        try
        {
            _setLoginID();
            UserService _userService = new UserService(_db, _loginID);
            var _user = _userService.FindList();

            var _listUserVM = new ListUserViewModel();
            _listUserVM.UpdateTokenAndMessage(pListUserVM.t, pListUserVM.Message);

            if (_user != null && _user.Count > 0)
            {
                foreach (var _users in _user)
                {
                    _listUserVM.Users.Add(
                        new DetailsUserViewModel()
                        {
                            Email = _users.Email,
                            Username = _users.Username,
                            Active = (_users.Active ? "Aktif" : "Tidak Aktif"),
                            CreatedAt = _users.CreatedAt,
                            CreatedBy = _users.CreatedBy,
                            UpdatedAt = _users.UpdatedAt,
                            UpdatedBy = _users.UpdatedBy,
                        }
                    );
                }
            }



            return View("List", _listUserVM);
        }
        catch (Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
        }

        return View("List", pListUserVM);
    }

}
