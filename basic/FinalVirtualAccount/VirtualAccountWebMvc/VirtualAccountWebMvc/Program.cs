using Microsoft.EntityFrameworkCore;
using VirtualAccountServices;

var builder = WebApplication.CreateBuilder(args);

builder.Logging.ClearProviders();
builder.Logging.AddConsole();
//builder.Logging.AddDebug();


// Add services to the container.
builder.Services.AddDbContext<VirtualAccountServices.Models.VirtualAccountDbContext>(options =>
options.UseSqlServer("name=ConnectionStrings:VIRTUAL_ACCOUNT"));

//builder.Services.AddScoped<VirtualAccountServices.Services.VirtualAccountService>();

builder.Services.AddControllersWithViews();
//builder.Services.AddLogging();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

//app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");



app.Run();
