﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using VirtualAccountServices.Models;
using VirtualAccountServices.Services;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountTest
{
    public class TopUpServiceTest
    {
        [Fact]
        public void TestTopup_SeedingTopup()
        {
            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>().UseSqlServer(TestVars.ConnectionString).Options;

            using (var _db = new VirtualAccountDbContext(dbOption))
            {
                TopUpService _topupService = new TopUpService(_db);
                _topupService.Migrate();

                List<TopUpModel> _topups = new List<TopUpModel>();
                _topups.Add(new TopUpModel()
                {
                    VirtualAccountID = 123456,
                    TopUpAmount = 100000,
                    BankID = 1,
                    BankName = "BCA",
                    BankAccountNumber = 001,
                    BankAccountOwner = "faisal",
                });

                for (int i = 0; i < _topups.Count; i++)
                {
                    var _topup = _topups[i];
                    var _virtualAccountId = _topup.VirtualAccountID;
                    var _topupAmount = _topup.TopUpAmount;
                    var _bankId = _topup.BankID;
                    var _bankName = _topup.BankName;
                    var _bankAccountNumber = _topup.BankAccountNumber;
                    var _bankAccountOwner = _topup.BankAccountOwner;

                    var _topupID = _topupService.AddTopup(_virtualAccountId,
                        _topupAmount,
                        _bankId,
                        _bankName,
                        _bankAccountNumber,
                        _bankAccountOwner);


                    TopUpModel? _fTopUp = _topupService.FindTopUpByID(_topupID);
                    Assert.NotNull(_fTopUp);

                    if (_fTopUp != null)
                    {
                        Assert.Equal(_virtualAccountId, _fTopUp.VirtualAccountID);
                        Assert.Equal(_topupAmount, _fTopUp.TopUpAmount);
                        Assert.Equal(_bankId, _fTopUp.BankID);
                        Assert.Equal(_bankName, _fTopUp.BankName);
                        Assert.Equal(_bankAccountNumber, _fTopUp.BankAccountNumber);
                        Assert.Equal(_bankAccountOwner, _fTopUp.BankAccountOwner);
                    }
                }
            }
        }
    }
}
