﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualAccountServices;
using VirtualAccountServices.Models;
using Xunit;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountTest.Services
{
    public class UserServiceTest
    {
        [Fact]
        public void TestUser_NormalTest()
        {

            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
                 .UseSqlServer(TestVars.ConnectionString)
                 .Options;

            using (var _db = new VirtualAccountDbContext(dbOption))
            {

                UserService _userService = new UserService(_db,
                    TestVars.LoginEmail);
                _userService.Migrate();


                UserLoginService _userLoginService = new UserLoginService(_db,
                    TestVars.LoginEmail);
                _userLoginService.Migrate();


                var _email = "faisal@gmail.com";
                var _password = "123456";
                var _username = "Faisal";
                var _active = true;

                _userService.AddUser(_email,
                    _password,
                    _password,
                    _username,
                    _active);


                var _fUser = _userService.FindByEmail(_email, false);
                Assert.NotNull(_fUser);
                if (_fUser != null)
                {
                    Assert.Equal(_email, _fUser.Email);
                    Assert.Equal(_username, _fUser.Username);

                    var _createdAt = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd");
                    Assert.Equal(_createdAt, _fUser.CreatedAt.ToString("yyyy-MM-dd"));

                    Assert.Equal(TestVars.LoginEmail, _fUser.CreatedBy);

                }


                _username = "Username Update";
                _active = false;
                _userService.UpdateUser(_email,
                    _username,
                    _active);

                _fUser = _userService.FindByEmail(_email, false);
                Assert.NotNull(_fUser);
                if (_fUser != null)
                {
                    Assert.Equal(_email, _fUser.Email);
                    Assert.Equal(_username, _fUser.Username);

                    /*var _updatedAt = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd");
                    Assert.Equal(_updatedAt, _fAdminUser.UpdatedAt.ToString("yyyy-MM-dd"));*/

                    Assert.Equal(TestVars.LoginEmail, _fUser.UpdatedBy);

                }



                var _userLogin = _userLoginService.Login(_email, _password);

                var _fLogin = _userLoginService.FindByLoginID(_userLogin.LoginID);
                if (_fLogin != null)
                {
                    Assert.Equal(_email, _fLogin.User);
                    var _createdAt = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd");
                    Assert.Equal(_createdAt, _fLogin.CreatedAt.ToString("yyyy-MM-dd"));
                    Assert.Equal(false, _fLogin.Expired);
                }


                _userLoginService.Logout(_userLogin.LoginID);

                _fLogin = _userLoginService.FindByLoginID(_userLogin.LoginID);
                if (_fLogin != null)
                {
                    Assert.Equal(true, _fLogin.Expired);
                }

            }
        }
    }
}
