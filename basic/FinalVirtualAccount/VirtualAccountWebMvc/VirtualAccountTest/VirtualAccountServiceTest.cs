﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using VirtualAccountServices.Models;
using VirtualAccountServices.Services;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountTest
{
    public class VirtualAccountServiceTest
    {
        [Fact]
        public void TestVirtualAccount_SeedingVirtualAccount()
        {
            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>().UseSqlServer(TestVars.ConnectionString).Options;

            using (var _db = new VirtualAccountDbContext(dbOption))
            {
                VirtualAccountService _virtualAccountService = new VirtualAccountService(_db);
                _virtualAccountService.Migrate();

                List<VirtualAccountModel> _virtualAccounts = new List<VirtualAccountModel>();
                _virtualAccounts.Add(new VirtualAccountModel()
                {
                    Email = "faisal@gmail.com",
                    Password = "123456",
                    Username = "faisal",
                    MobilePhone = 081383422960,
                    Balance = 10000000,
                });

                for (int i = 0; i < _virtualAccounts.Count; i++)
                {
                    var _virtualAccount = _virtualAccounts[i];
                    var _email = _virtualAccount.Email;
                    var _password = _virtualAccount.Password;
                    var _username = _virtualAccount.Username;
                    var _mobilePhone = _virtualAccount.MobilePhone;
                    var _balance = _virtualAccount.Balance;

                    var _virtualAccountID = _virtualAccountService.AddVirtualAccount(_email,
                        _password,
                        _username,
                        _mobilePhone,
                        _balance);


                    VirtualAccountModel? _fVirtualAccount = _virtualAccountService.FindVirtualByID(_virtualAccountID);
                    Assert.NotNull(_fVirtualAccount);

                    if (_fVirtualAccount != null)
                    {
                        Assert.Equal(_email, _fVirtualAccount.Email);
                        Assert.Equal(_password, _fVirtualAccount.Password);
                        Assert.Equal(_username, _fVirtualAccount.Username);
                        Assert.Equal(_mobilePhone, _fVirtualAccount.MobilePhone);
                        Assert.Equal(_balance, _fVirtualAccount.Balance);
                    }
                }
            }
        }
    }
}
