﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using VirtualAccountServices.Models;
using VirtualAccountServices.Services;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountTest
{
    public class TransactionServiceTest
    {
        [Fact]
        public void TestTransaction_SeedingTransaction()
        {
            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>().UseSqlServer(TestVars.ConnectionString).Options;
            using (var _db = new VirtualAccountDbContext(dbOption))
            {
                TransactionService _transactionService = new TransactionService(_db);
                _transactionService.Migrate();

                List<TransactionModel> _transactions = new List<TransactionModel>();
                _transactions.Add(new TransactionModel()
                {
                    VirtualAccountID = 123456,
                    VAMerchantID = 546134,
                    TransactionAmount = 3000000,
                    ReferenceID = 54869875,
                });

                for (int i = 0; i < _transactions.Count; i++)
                {
                    var _transaction = _transactions[i];
                    var _virtualAccountId = _transaction.VirtualAccountID;
                    var _vaMerchantId = _transaction.VAMerchantID;
                    var _transactionAmount = _transaction.TransactionAmount;
                    var _referenceId = _transaction.ReferenceID;

                    var _transactionID = _transactionService.AddTransaction(_virtualAccountId,
                        _vaMerchantId,
                        _transactionAmount,
                        _referenceId);


                    TransactionModel? _fTransaction= _transactionService.FindTransactionByID(_transactionID);
                    Assert.NotNull(_fTransaction);

                    if (_fTransaction != null)
                    {
                        Assert.Equal(_virtualAccountId, _fTransaction.VirtualAccountID);
                        Assert.Equal(_vaMerchantId, _fTransaction.VAMerchantID);
                        Assert.Equal(_transactionAmount, _fTransaction.TransactionAmount);
                        Assert.Equal(_referenceId, _fTransaction.ReferenceID);
                    }

                }
            }
        }
    }
}
