﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using VirtualAccountServices.Models;
using VirtualAccountServices.Services;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountTest
{
    public class BankServiceTest
    {
        [Fact]
        public void TestBank_SeedingBank()
        {
            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>().UseSqlServer(TestVars.ConnectionString).Options;

            using (var _db = new VirtualAccountDbContext(dbOption))
            {
                BankService _bank_service = new BankService(_db);
                _bank_service.Migrate();
                BankModel[] _banks = new BankModel[]
                {
                    new BankModel("BCA"),
                    new BankModel("Mandiri"),
                    new BankModel("BNI"),
                };

                for (int i = 0; i < _banks.Length; i++)
                {
                    var _bank = _banks[i];
                    var _bankName = _bank.BankName;

                    var _bankId = _bank_service.AddBank(_bankName);

                    var _fBank = _bank_service.FindBankByBankID(_bankId);
                    Assert.NotNull(_fBank);
                    if (_fBank != null)
                    {
                        Assert.Equal(_bankName, _fBank.BankName);
                    }
                }
            }
        }
    }
}
