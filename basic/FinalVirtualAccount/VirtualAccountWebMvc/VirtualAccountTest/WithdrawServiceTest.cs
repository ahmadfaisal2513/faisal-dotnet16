﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using VirtualAccountServices.Models;
using VirtualAccountServices.Services;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountTest
{
    public class WithdrawServiceTest
    {
        [Fact]
        public void TestWithdraw_SeedingWithdraw()
        {
            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>().UseSqlServer(TestVars.ConnectionString).Options;

            using (var _db = new VirtualAccountDbContext(dbOption))
            {
                WithdrawService _withdrawService = new WithdrawService(_db);
                _withdrawService.Migrate();

                List<WithdrawModel> _withdraws = new List<WithdrawModel>();
                _withdraws.Add(new WithdrawModel()
                {
                    VirtualAccountID = 123456,
                    WithdrawAmount = 100000,
                    BankID = 1,
                    BankName = "BCA",
                    BankAccountNumber = 001,
                    BankAccountOwner = "faisal",
                });

                for (int i = 0; i < _withdraws.Count; i++)
                {
                    var _withdraw = _withdraws[i];
                    var _virtualAccountId = _withdraw.VirtualAccountID;
                    var _withdrawAmount = _withdraw.WithdrawAmount;
                    var _bankId = _withdraw.BankID;
                    var _bankName = _withdraw.BankName;
                    var _bankAccountNumber = _withdraw.BankAccountNumber;
                    var _bankAccountOwner = _withdraw.BankAccountOwner;

                    var _withdrawID = _withdrawService.AddWithdraw(_virtualAccountId,
                        _withdrawAmount,
                        _bankId,
                        _bankName,
                        _bankAccountNumber,
                        _bankAccountOwner);


                    WithdrawModel? _fWithdraw = _withdrawService.FindWithdrawByID(_withdrawID);
                    Assert.NotNull(_fWithdraw);

                    if (_fWithdraw != null)
                    {
                        Assert.Equal(_virtualAccountId, _fWithdraw.VirtualAccountID);
                        Assert.Equal(_withdrawAmount, _fWithdraw.WithdrawAmount);
                        Assert.Equal(_bankId, _fWithdraw.BankID);
                        Assert.Equal(_bankName, _fWithdraw.BankName);
                        Assert.Equal(_bankAccountNumber, _fWithdraw.BankAccountNumber);
                        Assert.Equal(_bankAccountOwner, _fWithdraw.BankAccountOwner);
                    }
                }
            }
        }
    }
}
