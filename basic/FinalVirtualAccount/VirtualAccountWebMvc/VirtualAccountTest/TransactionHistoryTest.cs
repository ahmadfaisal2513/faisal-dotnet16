﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using VirtualAccountServices.Models;
using VirtualAccountServices.Services;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountTest
{
    public class TransactionHistoryTest
    {
        [Fact]
        public void TestTransactionHistory_SeedingTransactionHistory()
        {
            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>().UseSqlServer(TestVars.ConnectionString).Options;
            using (var _db = new VirtualAccountDbContext(dbOption))
            {
                TransactionHistoryService _transactionHistoryService = new TransactionHistoryService(_db);
                _transactionHistoryService.Migrate();

                List<TransactionHistoryModel> _transactionHistorys = new List<TransactionHistoryModel>();
                _transactionHistorys.Add(new TransactionHistoryModel()
                {
                    AccountID = 123456,
                    TransactionID = 546134,
                    Amount = 3000000,
                    TransactionType ="Transfer",
                });

                for (int i = 0; i < _transactionHistorys.Count; i++)
                {
                    var _transactionHistory = _transactionHistorys[i];
                    var _accountId = _transactionHistory.AccountID;
                    var _transactionId = _transactionHistory.TransactionID;
                    var _amount = _transactionHistory.Amount;
                    var _transactionType = _transactionHistory.TransactionType;

                    var _historyID = _transactionHistoryService.AddTransactionHistory(_accountId,
                        _transactionId,
                        _amount,
                        _transactionType);


                    TransactionHistoryModel? _fHistory = _transactionHistoryService.FindHistoryByID(_historyID);
                    Assert.NotNull(_fHistory);

                    if (_fHistory != null)
                    {
                        Assert.Equal(_accountId, _fHistory.AccountID);
                        Assert.Equal(_transactionId, _fHistory.TransactionID);
                        Assert.Equal(_amount, _fHistory.Amount);
                        Assert.Equal(_transactionType, _fHistory.TransactionType);
                    }

                }
            }
        }
    }
}
