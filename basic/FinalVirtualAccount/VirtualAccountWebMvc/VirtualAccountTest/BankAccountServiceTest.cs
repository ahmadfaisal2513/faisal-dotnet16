﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using VirtualAccountServices.Models;
using VirtualAccountServices.Services;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountTest
{
    public class BankAccountServiceTest
    {
        [Fact]
        public void TestBankAccount_SeedingBankAccount()
        {
            var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>()
              .UseSqlServer(TestVars.ConnectionString)
              .Options;

            using (var _db = new VirtualAccountDbContext(dbOption))
            {
                BankAccountService _bankAccountService = new BankAccountService(_db);
                _bankAccountService.Migrate();
                BankAccountModel[] _accounts = new BankAccountModel[]
                {
                    new BankAccountModel(123456,1,001,"faisal",1000000),
                    new BankAccountModel(456789,2,002,"Ibrahim",3000000),
                    new BankAccountModel(859764,3,003,"Ronaldo",5000000),
                };


                for (int i = 0; i < _accounts.Length; i++)
                {

                    var _account = _accounts[i];
                    var _virtualAccountId = _account.VirtualAccountID;
                    var _bankId = _account.BankID;
                    var _bankAccountNumber = _account.BankAccountNumber;
                    var _bankAccountOwner = _account.BankAccountOwner;
                    var _balance = _account.Balance;

                    var _bankAccountID = _bankAccountService.AddBankAccount(_virtualAccountId,
                        _bankId,
                        _bankAccountNumber,
                        _bankAccountOwner,
                        _balance);

                    var _fBankAccount = _bankAccountService.FindByID(_bankAccountID);
                    Assert.NotNull(_fBankAccount);
                    if (_fBankAccount != null)
                    {
                        Assert.Equal(_bankId, _fBankAccount.BankID);
                        Assert.Equal(_bankAccountNumber, _fBankAccount.BankAccountNumber);
                        Assert.Equal(_bankAccountOwner, _fBankAccount.BankAccountOwner);
                        Assert.Equal(_balance, _fBankAccount.Balance);
                    }
                }
            }
        }
    }
}
