﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using VirtualAccountServices.Models;
using VirtualAccountServices.Services;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountTest
{
    public class TransferServiceTest
    {
        [Fact]
        public void TestTransfer_SeedingTransfer()
        {
                var dbOption = new DbContextOptionsBuilder<VirtualAccountDbContext>().UseSqlServer(TestVars.ConnectionString).Options;

                using (var _db = new VirtualAccountDbContext(dbOption))
                {
                    TransferService _transferService = new TransferService(_db);
                    _transferService.Migrate();

                    List<TransferModel> _transfers = new List<TransferModel>();
                    _transfers.Add(new TransferModel()
                    {
                        FromVirtualAccountID = 123456,
                        ToVirtualAccountID = 23456,
                        TransferAmount = 1000000,
                    });

                    for (int i = 0; i < _transfers.Count; i++)
                    {
                        var _transfer = _transfers[i];
                        var _fromVirtualAccountId = _transfer.FromVirtualAccountID;
                        var _toVirtualAccountId = _transfer.ToVirtualAccountID;
                        var _transferAmount = _transfer.TransferAmount;

                        var _transferID = _transferService.AddTransfer(_fromVirtualAccountId,
                            _toVirtualAccountId,
                            _transferAmount);


                        TransferModel? _fTransfer = _transferService.FindTransferByID(_transferID);
                        Assert.NotNull(_fTransfer);

                        if (_fTransfer != null)
                        {
                            Assert.Equal(_fromVirtualAccountId, _fTransfer.FromVirtualAccountID);
                            Assert.Equal(_toVirtualAccountId, _fTransfer.ToVirtualAccountID);
                            Assert.Equal(_transferAmount, _fTransfer.TransferAmount);
                        }
                    }
                }
            }
        }
    }
