﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace VirtualAccountServices.Models
{
    [Table("transaction_history")]
    public class TransactionHistoryModel
    {
        [Key]
        [Column("history_id", TypeName = "bigint")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long HistoryID { get; set; }

        [Column("account_id", TypeName = "bigint")]
        public long AccountID { get; set; }

        [Column("transaction_id", TypeName = "bigint")]
        public long TransactionID { get; set; }

        [Column("history_date", TypeName = "datetime")]
        public DateTime HistoryDate { get; set; }

        [Column("amount", TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }

        [Column("transaction_type", TypeName = "varchar(30)")]
        public string TransactionType { get; set; }


        public TransactionHistoryModel() { }

        public TransactionHistoryModel(long accountId,
            long transactionId,
            DateTime historyDate,
            decimal amount,
            string transactionType)
        {
            this.AccountID = accountId;
            this.TransactionID = transactionId;
            this.HistoryDate = historyDate;
            this.Amount = amount;
            this.TransactionType = transactionType;
        }
    }
}
