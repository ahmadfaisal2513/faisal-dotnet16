﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace VirtualAccountServices.Models
{
    [Table("withdraw")]
    public class WithdrawModel
    {
        [Key]
        [Column("withdraw_id", TypeName = "bigint")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long WithdrawID { get; set; }

        [Column("virtual_account_id", TypeName = "bigint")]
        public long VirtualAccountID { get; set; }

        [Column("withdraw_amount", TypeName = "decimal(18,2)")]
        public decimal WithdrawAmount { get; set; }

        [Column("withdraw_date", TypeName = "datetime")]
        public DateTime WithdrawDate { get; set; }

        [Column("bank_id", TypeName = "bigint")]
        public long BankID { get; set; }

        [Column("bank_name", TypeName = "varchar(60)")]
        public string BankName { get; set; }

        [Column("bank_account_number", TypeName = "bigint")]
        public long BankAccountNumber { get; set; }

        [Column("bank_account_owner", TypeName = "varchar(60)")]
        public string BankAccountOwner { get; set; }
    }
}
