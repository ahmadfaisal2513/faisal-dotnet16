﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace VirtualAccountServices.Models
{
    [Table("transactions")]
    public class TransactionModel
    {
        [Key]
        [Column("transaction_id", TypeName = "bigint")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TransactionID { get; set; }

        [Column("transaction_date", TypeName = "datetime")]
        public DateTime TransactionDate { get; set; }

        [Column("virtual_account_id", TypeName = "bigint")]
        public long VirtualAccountID { get; set; }

        [Column("va_merchant_id", TypeName = "bigint")]
        public long VAMerchantID { get; set; }

        [Column("transaction_amount", TypeName = "decimal(18,2)")]
        public decimal TransactionAmount { get; set; }

        [Column("reference_id", TypeName = "bigint")]
        public long ReferenceID { get; set; }


        public TransactionModel() { }

        public TransactionModel(DateTime transactionDate,
            long virtualAccountId,
            long vaMerchantId,
            decimal transactionAmount,
            long referenceId)
        {
            this.TransactionDate = transactionDate;
            this.VirtualAccountID = virtualAccountId;
            this.VAMerchantID = vaMerchantId;
            this.TransactionAmount = transactionAmount;
            this.ReferenceID = referenceId;
        }
    }
}
