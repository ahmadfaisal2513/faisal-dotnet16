﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace VirtualAccountServices.Models
{
    [Table("login")]
    public class UserLoginModel
    {
        [Key]
        [Column("login_id", TypeName = "varchar(100)")]
        public string LoginID { get; set; } = string.Empty;

        [Column("user", TypeName = "varchar(60)")]
        public string User { get; set; } = string.Empty;

        [Column("created_at", TypeName = "datetime")]
        public DateTime CreatedAt { get; set; }

        [Column("expired", TypeName = "bit")]
        public bool Expired { get; set; }
    }
}
