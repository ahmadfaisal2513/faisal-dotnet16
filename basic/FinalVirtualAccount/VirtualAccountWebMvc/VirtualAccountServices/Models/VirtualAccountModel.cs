﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace VirtualAccountServices.Models
{
    [Table("virtual_account")]
    public class VirtualAccountModel
    {
        [Key]
        [Column("virtual_account_id", TypeName = "bigint")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long VirtualAccountID { get; set; }

        [Column("email", TypeName = "varchar(60)")]
        public string Email { get; set; }

        [Column("password", TypeName = "varchar(60)")]
        public string Password { get; set; }

        [Column("username", TypeName = "varchar(60)")]
        public string Username { get; set; }

        [Column("mobilephone", TypeName = "bigint")]
        public long MobilePhone { get; set; }

        [Column("balance", TypeName = "decimal(18,2)")]
        public decimal Balance { get; set; }



        public VirtualAccountModel() { }

        public VirtualAccountModel(string email,
            string password,
            string username,
            long mobilePhone,
            decimal balance)
        {
            this.Email = email;
            this.Password = password;
            this.Username = username;
            this.MobilePhone = mobilePhone;
            this.Balance = balance;
        }
    }
}
