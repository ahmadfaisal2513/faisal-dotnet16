﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace VirtualAccountServices.Models
{
    [Table("bank_account")]
    public class BankAccountModel
    {
        [Key]
        [Column("bank_account_id", TypeName = "bigint")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long BankAccountID { get; set; }

        [Column("virtual_account_id", TypeName = "bigint")]
        public long VirtualAccountID { get; set; }

        [Column("bank_id", TypeName = "bigint")]
        public long BankID { get; set; }

        [Column("bank_account_number", TypeName = "bigint")]
        public long BankAccountNumber { get; set; }

        [Column("bank_account_owner", TypeName = "varchar(30)")]
        public string BankAccountOwner { get; set; }

        [Column("Balance", TypeName = "decimal(18,2)")]
        public decimal Balance { get; set; }


        public BankAccountModel() { }

        public BankAccountModel(long virtualAccountId,
            long bankId,
            long bankAccountNumber,
            string bankAccountOwner,
            decimal balance)
        {
            this.VirtualAccountID = virtualAccountId;
            this.BankID = bankId;
            this.BankAccountNumber = bankAccountNumber;
            this.BankAccountOwner = bankAccountOwner;
            this.Balance = balance;
        }
    }
}
