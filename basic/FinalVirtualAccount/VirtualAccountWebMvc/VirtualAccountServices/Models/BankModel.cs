﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace VirtualAccountServices.Models
{
    [Table("bank")]
    public class BankModel
    {
        [Key]
        [Column("bank_id", TypeName = "bigint")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long BankID { get; set; }

        [Column("bank_name", TypeName = "varchar(30)")]
        public string BankName { get; set; }

        public BankModel()
        {
        }
        public BankModel(string bankName)
        {
            this.BankName = bankName;
        }
    }
}
