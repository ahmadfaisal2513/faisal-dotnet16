﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace VirtualAccountServices.Models
{
    [Table("top_up")]
    public class TopUpModel
    {
        [Key]
        [Column("topup_id", TypeName = "bigint")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TopUpID { get; set; }

        [Column("virtual_account_id", TypeName = "bigint")]
        public long VirtualAccountID { get; set; }

        [Column("topup_amount", TypeName = "decimal(18,2)")]
        public decimal TopUpAmount { get; set; }

        [Column("topup_date", TypeName = "datetime")]
        public DateTime TopUpDate { get; set; }

        [Column("bank_id", TypeName = "bigint")]
        public long BankID { get; set; }

        [Column("bank_name", TypeName = "varchar(30)")]
        public string BankName { get; set; }

        [Column("bank_account_number", TypeName = "bigint")]
        public long BankAccountNumber { get; set; }

        [Column("bank_account_owner", TypeName = "varchar(30)")]
        public string BankAccountOwner { get; set; }


        public TopUpModel() { }

        public TopUpModel(long virtualAccountId,
            decimal topupAmount,
            DateTime topupDate,
            long bankId,
            string bankName,
            long bankAccountNumber,
            string bankAccountOwner)
        {
            this.VirtualAccountID = virtualAccountId;
            this.TopUpAmount = topupAmount;
            this.TopUpDate = topupDate;
            this.BankID = bankId;
            this.BankName = bankName;
            this.BankAccountNumber = bankAccountNumber;
            this.BankAccountOwner = bankAccountOwner;
        }
    }
}
