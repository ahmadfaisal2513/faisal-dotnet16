﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;


namespace VirtualAccountServices.Models
{
    public class VirtualAccountDbContext : DbContext
    {
        /* tables */
        public DbSet<BankModel> Banks { get; set; }

        public DbSet<BankAccountModel> BankAccounts { get; set; }

        public DbSet<TopUpModel> Topups { get; set; }

        public DbSet<TransactionHistoryModel> TransactionHistorys { get; set; }

        public DbSet<TransactionModel> Transactions { get; set; }

        public DbSet<TransferModel> Transfers { get; set; }

        public DbSet<VirtualAccountModel> VirtualAccounts { get; set; }

        public DbSet<WithdrawModel> Withdraws { get; set; }

        public DbSet<UserModel> Users { get; set; }

        public DbSet<UserLoginModel> UserLogins { get; set; }

        /*        constructors*/
        public VirtualAccountDbContext(DbContextOptions<VirtualAccountDbContext> dbContextOptions) : base(dbContextOptions)
        {
        }

        private string _connString;
        public VirtualAccountDbContext(string connectionString)
        {
            _connString = connectionString;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BankModel>();

            modelBuilder.Entity<BankAccountModel>();

            modelBuilder.Entity<TopUpModel>();

            modelBuilder.Entity<TransactionHistoryModel>();

            modelBuilder.Entity<TransactionModel>();

            modelBuilder.Entity<TransferModel>();

            modelBuilder.Entity<VirtualAccountModel>();

            modelBuilder.Entity<WithdrawModel>();

            modelBuilder.Entity<UserModel>();

            modelBuilder.Entity<UserLoginModel>();
        }
    }

    /*Configurasi DbContext*/
    public class AirlineBookingDbContextFactory : IDesignTimeDbContextFactory<VirtualAccountDbContext>
    {
        public VirtualAccountDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<VirtualAccountDbContext>();

            optionsBuilder.UseSqlServer("Server=localhost\\SQLEXPRESS;Database=VIRTUAL_ACCOUNT;User Id=sa;Password=123456;");

            return new VirtualAccountDbContext(optionsBuilder.Options);
        }
    }
}

