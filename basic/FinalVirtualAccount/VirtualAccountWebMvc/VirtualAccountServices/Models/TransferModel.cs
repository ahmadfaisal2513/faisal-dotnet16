﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace VirtualAccountServices.Models
{
    [Table("transfer")]
    public class TransferModel
    {
        [Key]
        [Column("transfer_id", TypeName = "bigint")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TransferID { get; set; }

        [Column("transfer_date", TypeName = "datetime")]
        public DateTime TransferDate { get; set; }

        [Column("from_virtual_accoount_id", TypeName = "bigint")]
        public long FromVirtualAccountID { get; set; }

        [Column("to_virtual_account_id", TypeName = "bigint")]
        public long ToVirtualAccountID { get; set; }

        [Column("transfer_amount", TypeName = "decimal(18,2)")]
        public decimal TransferAmount { get; set; }


        public TransferModel() { }

        public TransferModel(DateTime transferDate,
            long fromVirtualAccountId,
            long toVirtualAccountId,
            decimal transferAmount)
        {
            this.TransferDate = transferDate;
            this.FromVirtualAccountID = fromVirtualAccountId;
            this.ToVirtualAccountID = toVirtualAccountId;
            this.TransferAmount = transferAmount;
        }
    }
}
