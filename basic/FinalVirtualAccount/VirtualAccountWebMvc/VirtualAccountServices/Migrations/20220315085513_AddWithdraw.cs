﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VirtualAccountServices.Migrations
{
    public partial class AddWithdraw : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "withdraw",
                columns: table => new
                {
                    withdraw_id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    virtual_account_id = table.Column<long>(type: "bigint", nullable: false),
                    withdraw_amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    withdraw_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    bank_id = table.Column<long>(type: "bigint", nullable: false),
                    bank_name = table.Column<string>(type: "varchar(60)", nullable: false),
                    bank_account_number = table.Column<long>(type: "bigint", nullable: false),
                    bank_account_owner = table.Column<string>(type: "varchar(60)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_withdraw", x => x.withdraw_id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "withdraw");
        }
    }
}
