﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VirtualAccountServices.Migrations
{
    public partial class AddupdateTransfer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "tranfers_amount",
                table: "transfer",
                newName: "transfer_amount");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "transfer_amount",
                table: "transfer",
                newName: "tranfers_amount");
        }
    }
}
