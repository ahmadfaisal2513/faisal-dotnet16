﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VirtualAccountServices.Migrations
{
    public partial class AddUbahRegister : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_register_user",
                table: "register_user");

            migrationBuilder.RenameTable(
                name: "register_user",
                newName: "register");

            migrationBuilder.AddPrimaryKey(
                name: "PK_register",
                table: "register",
                column: "email");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_register",
                table: "register");

            migrationBuilder.RenameTable(
                name: "register",
                newName: "register_user");

            migrationBuilder.AddPrimaryKey(
                name: "PK_register_user",
                table: "register_user",
                column: "email");
        }
    }
}
