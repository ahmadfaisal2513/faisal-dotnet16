﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VirtualAccountServices.Migrations
{
    public partial class AddTopUp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "top_up",
                columns: table => new
                {
                    topup_id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    virtual_account_id = table.Column<long>(type: "bigint", nullable: false),
                    topup_amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    topup_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    bank_id = table.Column<long>(type: "bigint", nullable: false),
                    bank_name = table.Column<string>(type: "varchar(30)", nullable: false),
                    bank_account_number = table.Column<long>(type: "bigint", nullable: false),
                    bank_account_owner = table.Column<string>(type: "varchar(30)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_top_up", x => x.topup_id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "top_up");
        }
    }
}
