﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VirtualAccountServices.Migrations
{
    public partial class AddUpdateTransferAmount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "tranfer_amount",
                table: "transfer",
                newName: "tranfers_amount");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "tranfers_amount",
                table: "transfer",
                newName: "tranfer_amount");
        }
    }
}
