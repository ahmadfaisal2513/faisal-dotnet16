﻿using VirtualAccountServices.Models;
using VirtualAccountServices.Services;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;


namespace VirtualAccountServices
{

    public class UserLoginService : BaseService
    {
        public UserLoginService(VirtualAccountDbContext db,
            string loginEmail) : base(db, loginEmail)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE login;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }


        private string _generateToken(string UserEmail)
        {
            var rawToken = UserEmail + Helpers.IDHelper.ID().ToString();
            return HashService.GenerateHash(rawToken);
        }


        public Models.UserLoginModel _addLogin(string user)
        {
            if (string.IsNullOrEmpty(user))
            {
                throw new Exception("invalid_user");
            }

            var _adminLogin = new Models.UserLoginModel();
            _adminLogin.CreatedAt = DateTime.Now.ToUniversalTime();
            _adminLogin.LoginID = _generateToken(user);
            _adminLogin.User = user;
            _adminLogin.Expired = false;

            _db.UserLogins.Add(_adminLogin);
            _db.SaveChanges();

            return _adminLogin;
        }

        public Models.UserLoginModel FindByLoginID(string loginID)
        {
            var _login = _db.UserLogins.Where(x => x.LoginID == loginID)
                .OrderByDescending(x => x.CreatedAt)
                .FirstOrDefault();
            if (_login == null)
            {
                throw new Exception("invalid_login_session");
            }
            return _login;
        }

        private Models.UserLoginModel? _findByemail(string email)
        {
            return _db.UserLogins.Where(x => x.User == email)
                .OrderByDescending(x => x.CreatedAt)
                .FirstOrDefault();

        }

        public Models.UserLoginModel Login(string email,
            string password)
        {

            VirtualAccountService _userService = new VirtualAccountService(_db, _loginEmail);

            var _passengerUser = _userService.FindVirtualByID(email, false);

            if (!HashService.CompareHash(password, _passengerUser.Password))
            {
                throw new Exception("invalid_password");
            }

            var _fLogin = _findByemail(email);
            if (_fLogin == null)
            {
                var _login = _addLogin(email);
                return _login;

            }
            else
            {
                if (_fLogin.Expired == false)
                {
                    return _fLogin;

                }
                else
                {
                    var _login = _addLogin(email);
                    return _login;
                }

            }

        }


        public void Logout(string loginID)
        {
            var _login = FindByLoginID(loginID);
            if (_login != null)
            {
                _login.Expired = true;
                _db.SaveChanges();

            }
        }


    }
}
