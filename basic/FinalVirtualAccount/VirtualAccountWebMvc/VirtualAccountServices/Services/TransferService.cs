﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountServices.Services
{
    public class TransferService : BaseService
    {
        public TransferService(VirtualAccountDbContext db) : base(db)
        {
        }
        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE transfer;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }
        private void validateBase(long fromVirtualAccountId,
            long toVirtualAccountId,
            decimal transferAmount)
        {
            if (fromVirtualAccountId == null)
            {
                throw new Exception("from_virtual_account_id_kosong");
            }
            if (toVirtualAccountId == null)
            {
                throw new Exception("to_virtual_account_id_kosong");
            }
            if (transferAmount <= 0)
            {
                throw new Exception("transfer_amount_kosong");
            }
        }

        private void validateID(long transferID)
        {
            if (transferID == null)
            {
                throw new Exception("invalid_transfer_id");
            }
        }

        private void setValue(ref TransferModel _transfer,
            long fromVirtualAccountId,
            long toVirtualAccountId,
            decimal transferAmount)
        {
            _transfer.TransferDate = DateTime.Now;
            _transfer.FromVirtualAccountID = fromVirtualAccountId;
            _transfer.ToVirtualAccountID = toVirtualAccountId;
            _transfer.TransferAmount = transferAmount;
        }

        public long AddTransfer(long fromVirtualAccountId,
            long toVirtualAccountId,
            decimal transferAmount)
        {
            validateBase(fromVirtualAccountId,
                toVirtualAccountId,
                transferAmount);

            var _transfer = new TransferModel();

            setValue(ref _transfer,
                fromVirtualAccountId,
                toVirtualAccountId,
                transferAmount);

            _db.Transfers.Add(_transfer);
            _db.SaveChanges();

            return _transfer.TransferID;
        }

        public long UpdateTransfer(long transferID,
            long fromVirtualAccountId,
            long toVirtualAccountId,
            decimal transferAmount)
        {
            validateID(transferID);

            this.validateBase(fromVirtualAccountId,
                toVirtualAccountId,
                transferAmount);

            var _fTransfer = new TransferModel();

            setValue(ref _fTransfer,
                fromVirtualAccountId,
                toVirtualAccountId,
                transferAmount);

            _db.SaveChanges();

            return transferID;
        }

        public void DeleteTransfer(long transferID)
        {
            validateID(transferID);

            var _fTransfer = FindByID(transferID);
            if (_fTransfer != null)
            {
                _db.Transfers.Remove(_fTransfer);
                _db.SaveChanges();
            }
        }

        public TransferModel? FindByID(long transferID)
        {
            var _fTransfer = _db.Transfers.Where(x => x.TransferID == transferID).FirstOrDefault();
            if (_fTransfer == null)
            {
                throw new Exception("Transfer_not_found");
            }
            return _fTransfer;
        }

        public TransferModel? FindTransferByID(long transferID)
        {
            return FindByID(transferID);
        }

        public List<TransferModel> GetTransfers()
        {
            return _db.Transfers.ToList();
        }
    }
}
