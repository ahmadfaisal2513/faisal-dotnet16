﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace VirtualAccountServices.Services
{
    public class TopUpService : BaseService
    {

        private CultureInfo _cultureInfo;

        public TopUpService(VirtualAccountDbContext db, string _loginID) : base(db)
        {
            _cultureInfo = CultureInfo.InvariantCulture;
        }
        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE top_up;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        /* private DateTime[] parseTopupDate(string topupDate)
         {
             DateTime[] result = new DateTime[1];
             result[0] = DateTime.ParseExact(topupDate, "yyyy-MM-dd hh:mm tt", _cultureInfo);
             return result;
         }*/

        private void validateBase(long virtualAccountId,
           decimal topupAmount,
           /*           string topupDate,*/
           long bankId,
           string bankName,
           long bankAccountNumber,
           string bankAccountOwner)
        {
            if (virtualAccountId == null)
            {
                throw new Exception("virtual_account_id_kosong");
            }
            if (topupAmount <= 0)
            {
                throw new Exception("topup_amount_kosong");
            }
            /*            if (string.IsNullOrEmpty(topupDate))
                        {
                            throw new Exception("topup_date_kosong");
                        }*/
            if (bankId == null)
            {
                throw new Exception("bank_id_kosong");
            }
            if (string.IsNullOrEmpty(bankName))
            {
                throw new Exception("bank_name_kosong");
            }
            if (bankAccountNumber == null)
            {
                throw new Exception("bank_account_number_kosong");
            }
            if (string.IsNullOrEmpty(bankAccountOwner))
            {
                throw new Exception("bank_account_owner_kosong");
            }
        }

        private void validateID(long topupID)
        {
            if (topupID == null)
            {
                throw new Exception("invalid_Topup_id");
            }
        }

        private void setValue(ref TopUpModel _topUp,
           long virtualAccountId,
           decimal topupAmount,
           /*           DateTime topupDate,*/
           long bankId,
           string bankName,
           long bankAccountNumber,
           string bankAccountOwner)
        {
            _topUp.VirtualAccountID = virtualAccountId;
            _topUp.TopUpAmount = topupAmount;
            _topUp.TopUpDate = DateTime.Now;
            _topUp.BankID = bankId;
            _topUp.BankName = bankName;
            _topUp.BankAccountNumber = bankAccountNumber;
            _topUp.BankAccountOwner = bankAccountOwner;
        }

        public long AddTopup(long virtualAccountId,
            decimal topupAmount,
            /*            string topupDate,*/
            long bankId,
            string bankName,
            long bankAccountNumber,
            string bankAccountOwner)
        {
            validateBase(virtualAccountId,
                topupAmount,
                /*                topupDate,*/
                bankId,
                bankName,
                bankAccountNumber,
                bankAccountOwner);

            /*            var dt = parseTopupDate(topupDate);
                        DateTime _topupDate = dt[0];*/

            var _topup = new TopUpModel();

            setValue(ref _topup,
                virtualAccountId,
                    topupAmount,
                    /*                    _topupDate,*/
                    bankId,
                    bankName,
                    bankAccountNumber,
                    bankAccountOwner);

            _db.Topups.Add(_topup);
            _db.SaveChanges();

            return _topup.TopUpID;
        }

        public long UpdateTopup(long topupID,
            long virtualAccountId,
            decimal topupAmount,
            /*            string topupDate,*/
            long bankId,
            string bankName,
            long bankAccountNumber,
            string bankAccountOwner)
        {

            validateID(topupID);

            this.validateBase(virtualAccountId,
                topupAmount,
                /*                topupDate,*/
                bankId,
                bankName,
                bankAccountNumber,
                bankAccountOwner);

            /*            var dt = parseTopupDate(topupDate);
                        DateTime _topupDate = dt[0];*/

            var _fTopup = new TopUpModel();

            setValue(ref _fTopup,
                virtualAccountId,
                    topupAmount,
                    /*                    _topupDate,*/
                    bankId,
                    bankName,
                    bankAccountNumber,
                    bankAccountOwner);

            _db.SaveChanges();

            return topupID;
        }

        public void DeleteTopup(long topupID)
        {
            validateID(topupID);

            var _fTopup = FindByID(topupID);
            if (_fTopup != null)
            {
                _db.Topups.Remove(_fTopup);
                _db.SaveChanges();
            }
        }

        public TopUpModel? FindByID(long topupID)
        {
            var _fTopup = _db.Topups.Where(x => x.TopUpID == topupID).FirstOrDefault();
            if (_fTopup == null)
            {
                throw new Exception("topup_not_found");
            }
            return _fTopup;
        }


        public TopUpModel? FindTopUpByID(long topupID)
        {
            return FindByID(topupID);
        }

        public List<TopUpModel> GetTopups()
        {
            return _db.Topups.ToList();
        }
    }
}