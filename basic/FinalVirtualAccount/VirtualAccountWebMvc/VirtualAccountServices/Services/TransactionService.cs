﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountServices.Services
{
    public class TransactionService : BaseService
    {
        public TransactionService(VirtualAccountDbContext db) : base(db)
        {
        }
        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE transactions;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        private void validateBase(long virtualAccountId,
           long vaMerchantId,
           decimal transactionAmount,
           long referenceId)
        {
            if (virtualAccountId == null)
            {
                throw new Exception("virtual_account_id_kosong");
            }
            if (vaMerchantId == null)
            {
                throw new Exception("va_merchant_id_kosong");
            }
            if (transactionAmount <= 0)
            {
                throw new Exception("transaction_amount_kurang_dari_nol");
            }
            if (referenceId == null)
            {
                throw new Exception("reference_id_kosong");
            }
        }

        private void validateID(long transactionID)
        {
            if (transactionID == null)
            {
                throw new Exception("invalid_transaction_id");
            }
        }

        private void setValue(ref TransactionModel _transaction,
           long virtualAccountId,
           long vaMerchantId,
           decimal transactionAmount,
           long referenceId)
        {
            _transaction.TransactionDate = DateTime.Now;
            _transaction.VirtualAccountID = virtualAccountId;
            _transaction.VAMerchantID = vaMerchantId;
            _transaction.TransactionAmount = transactionAmount;
            _transaction.ReferenceID = referenceId ;
        }

        public long AddTransaction(long virtualAccountId,
            long vaMerchantId,
            decimal transactionAmount,
            long referenceId)
        {
            validateBase(virtualAccountId,
                vaMerchantId,
                transactionAmount,
                referenceId);

            var _transaction = new TransactionModel();

            setValue(ref _transaction,
                virtualAccountId,
                    vaMerchantId,
                    transactionAmount,
                    referenceId);

            _db.Transactions.Add(_transaction);
            _db.SaveChanges();

            return _transaction.TransactionID;
        }

        public long UpdateTransaction(long transactionID,
            long virtualAccountId,
            long vaMerchantId,
            decimal transactionAmount,
            long referenceId)
        {

            validateID(transactionID);

            this.validateBase(virtualAccountId,
                vaMerchantId,
                transactionAmount,
                referenceId);

            var _fTransaction = new TransactionModel();

            setValue(ref _fTransaction,
                virtualAccountId,
                    vaMerchantId,
                    transactionAmount,
                    referenceId);

            _db.SaveChanges();

            return transactionID;
        }

        public void DeleteTransaction(long transactionID)
        {
            validateID(transactionID);

            var _fTransaction = FindByID(transactionID);
            if (_fTransaction != null)
            {
                _db.Transactions.Remove(_fTransaction);
                _db.SaveChanges();
            }
        }

        public TransactionModel? FindByID(long transactionID)
        {
            var _fTransaction = _db.Transactions.Where(x => x.TransactionID == transactionID).FirstOrDefault();
            if (_fTransaction == null)
            {
                throw new Exception("Transaction_not_found");
            }
            return _fTransaction;
        }


        public TransactionModel? FindTransactionByID(long transactionID)
        {
            return FindByID(transactionID);
        }

        public List<TransactionModel> GetTransactionHistorys()
        {
            return _db.Transactions.ToList();
        }
    }
}
