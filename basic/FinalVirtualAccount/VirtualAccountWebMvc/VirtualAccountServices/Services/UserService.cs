﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;
using VirtualAccountServices.Services;


namespace VirtualAccountServices
{
    public class UserService : BaseService
    {
        public UserService(VirtualAccountDbContext db,
            string loginEmail) : base(db, loginEmail)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE register;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        private void validateRegister(string email,
            string password,
            string confirmPassword,
            string phone,
            string username)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("email_is_required");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new Exception("password_is_required");
            }

            if (string.IsNullOrEmpty(confirmPassword))
            {
                throw new Exception("confirm_password_is_required");
            }

            if (!password.Equals(confirmPassword))
            {
                throw new Exception("password_must_be_matched_with_confirm_password");
            }
            if (string.IsNullOrEmpty(phone))
            {
                throw new Exception("phone_is_required");
            }
            if (string.IsNullOrEmpty(username))
            {
                throw new Exception("username_is_required");
            }
        }

        private void setValue(ref Models.UserModel _user,
            string email,
            string password,
            string phone,
            string username,
            bool active
        )
        {
            _user.Email = email;

            var _hashPass = HashService.GenerateHash(password);
            _user.Password = _hashPass;
            _user.Username = username;
            _user.Phone = phone;
            _user.Active = active;
        }

        public void UpdateUser(string email, string username, bool active)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("email_is_required");
            }

            var _user = _findByEmail(email, true);
            if (_user != null)
            {
                _db.Users.Remove(_user);
                _db.SaveChanges();
            }
        }

        public object AddUser(string email, string password, string confirmPassword, string username, bool active)
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(string email,
            string phone,
            string username
            )
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("email_is_required");
            }
            if (string.IsNullOrEmpty(username))
            {
                throw new Exception("username_is_required");
            }
            if (string.IsNullOrEmpty(phone))
            {
                throw new Exception("phone_is_required");
            }

            var _user = _findByEmail(email, true);
            if (_user != null)
            {
                _user.UpdatedAt = DateTime.Now.ToUniversalTime();
                _user.UpdatedBy = _loginEmail;
                _user.Username = username;
                _user.Phone = phone;
                _db.SaveChanges();
            }
        }

        public Models.UserModel RegisterUser(string email,
            string password,
            string confirmPassword,
            string phone,
            string username)
        {
            validateRegister(email,
                password,
                confirmPassword,
                phone,
                username);

            var _user = new Models.UserModel();
            _user.CreatedAt = DateTime.Now.ToUniversalTime();
            _user.CreatedBy = _loginEmail;

            setValue(ref _user,
                email,
                password,
                phone,
                username,
                true);

            _db.Users.Add(_user);
            try
            {
                _db.SaveChanges();
            }
            catch (Exception e)
            {
                if (e.InnerException.Message.Contains("duplicate key"))
                {
                    throw new Exception("Email sudah terdaftar");
                }
                else
                {
                    throw;
                }
            }

            return _user;
        }

        private Models.UserModel _findByEmail(string email, bool noTracking)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException("email_is_required");
            }

            UserModel _fUser;
            if (noTracking)
            {
                _fUser = _db.Users
                    .FirstOrDefault(x => x.Email.ToLower().Equals(email.ToLower()));
            }
            else
            {
                _fUser = _db.Users.AsNoTracking()
                    .FirstOrDefault(x => x.Email.ToLower().Equals(email.ToLower()));
            }

            if (_fUser == null)
            {
                throw new Exception("Invalid User");
            }

            return _fUser;
        }

        public Models.UserModel FindByEmail(string email, bool noTracking)
        {
            return _findByEmail(email, noTracking);
        }

        public List<Models.UserModel> FindList()
        {
            return _db.Users.AsNoTracking().ToList();
        }
    }
}
