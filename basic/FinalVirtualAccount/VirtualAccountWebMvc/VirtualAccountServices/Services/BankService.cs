﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountServices.Services
{
    public class BankService : BaseService
    {
        public BankService(VirtualAccountDbContext db) : base(db)
        {
        }
        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE bank;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }
        private void validateBase(string bankName)
        {
            if (string.IsNullOrEmpty(bankName))
            {
                throw new Exception("bank_name_kosong");
            }
        }
        private void setValue(ref Models.BankModel _bank_id,
            string bankName)
        {
            _bank_id.BankName = bankName;
        }
        public long AddBank(string bankName)
        {

            validateBase(bankName);
            var _bank = new Models.BankModel();

            setValue(ref _bank, bankName);

            _db.Banks.Add(_bank);

            _db.SaveChanges();
            return _bank.BankID;

        }

        private Models.BankModel findByBankID(long bankId)
        {
            if (bankId == null)
            {
                throw new ArgumentNullException("bank_id_is_null");
            }

            var _fBankId = _db.Banks?.Where(x => x.BankID == bankId).FirstOrDefault();
            if (_fBankId == null)
            {
                throw new NullReferenceException("bank_not_found");
            }
            return _fBankId;
        }

        public Models.BankModel FindBankByBankID(long bankId)
        {
            return findByBankID(bankId);
        }
    }
}
