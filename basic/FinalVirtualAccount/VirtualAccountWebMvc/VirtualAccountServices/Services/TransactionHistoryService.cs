﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace VirtualAccountServices.Services
{
    public class TransactionHistoryService : BaseService
    {
        private CultureInfo _cultureInfo;

        public TransactionHistoryService(VirtualAccountDbContext db) : base(db)
        {
            _cultureInfo = CultureInfo.InvariantCulture;
        }
        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE transaction_history;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

 /*       private DateTime[] parseTopupDate(string historyDate)
        {
            DateTime[] result = new DateTime[1];
            result[0] = DateTime.ParseExact(historyDate, "yyyy-MM-dd hh:mm tt", _cultureInfo);
            return result;
        }*/

        private void validateBase(long accountId,
           long transactionId,
/*           string historyDate,*/
           decimal amount,
           string transactionType)
        {
            if (accountId == null)
            {
                throw new Exception("account_id_kosong");
            }
            if (transactionId == null)
            {
                throw new Exception("transaction_id_kosong");
            }
/*            if (string.IsNullOrEmpty(historyDate))
            {
                throw new Exception("history_date_kosong");
            }*/
            if (amount <= 0)
            {
                throw new Exception("amount_kurang_dari_nol");
            }
            if (string.IsNullOrEmpty(transactionType))
            {
                throw new Exception("transaction_type_kosong");
            }
        }

        private void validateID(long historyID)
        {
            if (historyID == null)
            {
                throw new Exception("invalid_history_id");
            }
        }

        private void setValue(ref TransactionHistoryModel _transactionHistory,
           long accountId,
           long transactionId,
/*           DateTime historyDate,*/
           decimal amount,
           string transactionType)
        {
            _transactionHistory.AccountID = accountId;
            _transactionHistory.TransactionID = transactionId;
            _transactionHistory.HistoryDate = DateTime.Now;
            _transactionHistory.Amount = amount;
            _transactionHistory.TransactionType = transactionType;
        }

        public long AddTransactionHistory(long accountId,
            long transactionId,
/*            string historyDate,*/
            decimal amount,
            string transactionType)
        {
            validateBase(accountId,
                transactionId,
/*                historyDate,*/
                amount,
                transactionType);

/*            var dt = parseHistoryDate(historyDate);
            DateTime _historyDate = dt[0];*/

            var _transactionhistory = new TransactionHistoryModel();

            setValue(ref _transactionhistory,
                accountId,
                    transactionId,
/*                    _historyDate,*/
                    amount,
                    transactionType);

            _db.TransactionHistorys.Add(_transactionhistory);
            _db.SaveChanges();

            return _transactionhistory.HistoryID;
        }

        public long UpdateTransactionHostory(long historyID,
            long accountId,
            long transactionId,
/*            string historyDate,*/
            decimal amount,
            string transactionType)
        {

            validateID(historyID);

            this.validateBase(accountId,
                transactionId,
/*                historyDate,*/
                amount,
                transactionType);

/*            var dt = parseHistoryDate(historyDate);
            DateTime _historyDate = dt[0];
*/
            var _fTransactionhistory = new TransactionHistoryModel();

            setValue(ref _fTransactionhistory,
                accountId,
                    transactionId,
/*                    _historyDate,*/
                    amount,
                    transactionType);

            _db.SaveChanges();

            return historyID;
        }

        public void DeleteTransactionHistory(long historyID)
        {
            validateID(historyID);

            var _fTransactionhistory = FindByID(historyID);
            if (_fTransactionhistory != null)
            {
                _db.TransactionHistorys.Remove(_fTransactionhistory);
                _db.SaveChanges();
            }
        }

        public TransactionHistoryModel? FindByID(long historyID)
        {
            var _fTransaction = _db.TransactionHistorys.Where(x => x.HistoryID == historyID).FirstOrDefault();
            if (_fTransaction == null)
            {
                throw new Exception("Transaction_not_found");
            }
            return _fTransaction;
        }


        public TransactionHistoryModel? FindHistoryByID(long historyID)
        {
            return FindByID(historyID);
        }

        public List<TransactionHistoryModel> GetTransactionHistorys()
        {
            return _db.TransactionHistorys.ToList();
        }
    }
}
