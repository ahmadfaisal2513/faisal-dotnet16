﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountServices.Services
{
    public class BankAccountService : BaseService
    {
        public BankAccountService(VirtualAccountDbContext db, string _loginID) : base(db)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE bank_account;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        private void validateBase(long virtualAccountId,
            long bankId,
            long bankAccountNumber,
            string bankAccountOwner,
            decimal balance)
        {
            if (virtualAccountId == null)
            {
                throw new ArgumentNullException("virtual_account_id_is_null");
            }
            if (bankId == null)
            {
                throw new ArgumentNullException("bank_id_is_null");
            }
            if (bankAccountNumber <= 0)
            {
                throw new ArgumentNullException("bank_account_number_is_null");
            }
            if (string.IsNullOrEmpty(bankAccountOwner))
            {
                throw new ArgumentNullException("bank_account_owner_is_null");
            }
            if (balance <= 0)
            {
                throw new ArgumentNullException("balance_is_null");
            }
        }

        private void setValue(ref BankAccountModel _bankAccount,
            long virtualAccountId,
            long bankId,
            long bankAccountNumber,
            string bankAccountOwner,
            decimal balance)
        {
            _bankAccount.VirtualAccountID = virtualAccountId;
            _bankAccount.BankID = bankId;
            _bankAccount.BankAccountNumber = bankAccountNumber;
            _bankAccount.BankAccountOwner = bankAccountOwner;
            _bankAccount.Balance = balance;
        }

        public long AddBankAccount(long virtualAccountId,
            long bankId,
            long bankAccountNumber,
            string bankAccountOwner,
            decimal balance)
        {
            validateBase(virtualAccountId,
                bankId,
                bankAccountNumber,
                bankAccountOwner,
                balance);

            var _bankAccount = new BankAccountModel();

            setValue(ref _bankAccount,
                virtualAccountId,
                bankId,
                bankAccountNumber,
                bankAccountOwner,
                balance);

            _db.BankAccounts.Add(_bankAccount);
            _db.SaveChanges();

            return _bankAccount.BankAccountID;
        }

        private BankAccountModel _findByID(long bankAccountID)
        {
            if (bankAccountID == null)
            {
                throw new ArgumentException("invalid_bank_account_id");
            }
            var _fBankAccount = _db.BankAccounts.Where(x => x.BankAccountID == bankAccountID).FirstOrDefault();
            if (_fBankAccount == null)
            {
                throw new NullReferenceException("bank_account_id_not_found");
            }
            return _fBankAccount;
        }

        public BankAccountModel FindByID(long bankAccountID)
        {
            return _findByID(bankAccountID);
        }

        public List<BankAccountModel> GetBankAccounts()
        {
            return _db.BankAccounts.ToList();
        }
    }
}
