﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountServices.Services
{
    public class VirtualAccountService : BaseService
    {
        public VirtualAccountService(VirtualAccountDbContext db) : base(db)
        {
        }
        public VirtualAccountService(VirtualAccountDbContext db,
           string loginEmail) : base(db, loginEmail)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE virtual_account;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        private void validateBase(string email,
            string password,
            string username ,
            long mobilePhone,
            decimal balance)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException("email_is_null");
            }
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("password_is_null");
            }
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException("username_is_null");
            }
            if (mobilePhone == null)
            {
                throw new ArgumentNullException("mobile_phone_is_null");
            }
            if (balance <= 0)
            {
                throw new ArgumentNullException("balance_is_null");
            }
        }

        private void setValue(ref VirtualAccountModel _user,
            string email,
            string password,
            string username
        )
        {
            _user.Email = email;

            var _hashPass = HashService.GenerateHash(password);
            _user.Password = _hashPass;
            _user.Username = username;
        }

        public void DeleteUser(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("email_is_required");
            }

            var _user = _findByID(email, true);
            if (_user != null)
            {
                _db.VirtualAccounts.Remove(_user);
                _db.SaveChanges();
            }
        }

        public void User(string email,
            string username)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("email_is_required");
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new Exception("username_is_required");
            }

            var _user = _findByID(email, true);
            if (_user != null)
            {
                _user.Username = username;
                _db.SaveChanges();
            }
        }

        private void validateID(long virtualAccountID)
        {
            if (virtualAccountID == null)
            {
                throw new Exception("invalid_virtual_account_id");
            }
        }

        private void setValue(ref VirtualAccountModel _virtualAccount,
            string email,
            string password,
            string username,
            long mobilePhone,
            decimal balance)
        {
            _virtualAccount.Email = email;
            _virtualAccount.Password = password;
            _virtualAccount.Username= username;
            _virtualAccount.MobilePhone = mobilePhone;
            _virtualAccount.Balance = balance;
        }

        public long AddVirtualAccount(string email,
            string password,
            string username,
            long mobilePhone,
            decimal balance)
        {
            validateBase(email,
                password,
                username,
                mobilePhone,
                balance);

            var _virtualAccount = new VirtualAccountModel();

            setValue(ref _virtualAccount,
                email,
                password,
                username,
                mobilePhone,
                balance);

            _db.VirtualAccounts.Add(_virtualAccount);
            _db.SaveChanges();

            return _virtualAccount.VirtualAccountID;
        }

        public long UpdateVirtualAccount(long virtualAccountID,
            string email,
            string password,
            string username,
            long mobilePhone,
            decimal balance)
        {
            validateID(virtualAccountID);

            this.validateBase(email,
                password,
                username,
                mobilePhone,
                balance); ;

            var _fVirtualAccount = new VirtualAccountModel();

            setValue(ref _fVirtualAccount,
                email,
                password,
                username,
                mobilePhone,
                balance);

            _db.SaveChanges();

            return virtualAccountID;
        }

 /*       public void DeleteVirtualAccount(long virtualAccountID)
        {
            validateID(virtualAccountID);

            var _fVirtualAccount = FindVirtualByID(virtualAccountID);
            if (_fVirtualAccount != null)
            {
                _db.VirtualAccounts.Remove(_fVirtualAccount);
                _db.SaveChanges();
            }
        }*/

        private VirtualAccountModel _findByID(string email, bool noTracking)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException("email_is_required");
            }
            VirtualAccountModel _fUser;
            if (noTracking)
            {
                _fUser = _db.VirtualAccounts
                    .FirstOrDefault(x => x.Email.ToLower().Equals(email.ToLower()));
            }
            else
            {
                _fUser = _db.VirtualAccounts.AsNoTracking()
                    .FirstOrDefault(x => x.Email.ToLower().Equals(email.ToLower()));
            }
            if (_fUser == null)
            {
                throw new Exception("Invalid User");
            }

            return _fUser;
        }

        public VirtualAccountModel FindVirtualByID(string email, bool noTracking)
        {
            return _findByID(email, noTracking);
        }

        public List<VirtualAccountModel> GetVirtualAccount()
        {
            return _db.VirtualAccounts.ToList();
        }
    }
}
