﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualAccountServices.Models;
using Microsoft.EntityFrameworkCore;

namespace VirtualAccountServices.Services
{
    public class WithdrawService : BaseService
    {
        public WithdrawService(VirtualAccountDbContext db) : base(db)
        {
        }
        
        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE witdraw;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }

        private void validateBase(long virtualAccountId,
            decimal withdrawAmount,
            long bankId,
            string bankName,
            long bankAccountNumber,
            string bankAccountOwner)
        {
            if (virtualAccountId == null)
            {
                throw new Exception("virtual_account_id_is_null");
            }
            if (withdrawAmount <= 0)
            {
                throw new Exception("withdraw_amount_is_null");
            }
            if (bankId == null)
            {
                throw new Exception("bank_id_is_null");
            }
            if (string.IsNullOrEmpty(bankName))
            {
                throw new Exception("bank_name_is_null");
            }
            if (bankAccountNumber == null)
            {
                throw new Exception("bank_account_number_is_null");
            }
            if (string.IsNullOrEmpty(bankAccountOwner))
            {
                throw new Exception("bank_account_owner_is_null");
            }
        }

        private void validateID(long withdrawID)
        {
            if (withdrawID == null)
            {
                throw new Exception("invalid_withdraw_id");
            }
        }

        private void setValue(ref WithdrawModel _withdraw,
            long virtualAccountId,
            decimal withdrawAmount,
            long bankId,
            string bankName,
            long bankAccountNumber,
            string bankAccountOwner)
        {
            _withdraw.VirtualAccountID = virtualAccountId;
            _withdraw.WithdrawAmount = withdrawAmount;
            _withdraw.WithdrawDate = DateTime.Now;
            _withdraw.BankID = bankId;
            _withdraw.BankName = bankName;
            _withdraw.BankAccountNumber = bankAccountNumber;
            _withdraw.BankAccountOwner = bankAccountOwner;
        }

        public long AddWithdraw(long virtualAccountId,
            decimal withdrawAmount,
            long bankId,
            string bankName,
            long bankAccountNumber,
            string bankAccountOwner)
        {
            validateBase(virtualAccountId,
                withdrawAmount,
                bankId,
                bankName,
                bankAccountNumber,
                bankAccountOwner);

            var _withdraw = new WithdrawModel();

            setValue(ref _withdraw,
                virtualAccountId,
                withdrawAmount,
                bankId,
                bankName,
                bankAccountNumber,
                bankAccountOwner);

            _db.Withdraws.Add(_withdraw);
            _db.SaveChanges();

            return _withdraw.WithdrawID;
        }

        public long UpdateWithdraw(long withdrawID,
            long virtualAccountId,
            decimal withdrawAmount,
            long bankId,
            string bankName,
            long bankAccountNumber,
            string bankAccountOwner)
        {
            validateID(withdrawID);

            this.validateBase(virtualAccountId,
                withdrawAmount,
                bankId,
                bankName,
                bankAccountNumber,
                bankAccountOwner);

            var _fWithdraw = new WithdrawModel();

            setValue(ref _fWithdraw,
                virtualAccountId,
                withdrawAmount,
                bankId,
                bankName,
                bankAccountNumber,
                bankAccountOwner);

            _db.SaveChanges();

            return withdrawID;
        }

        public void DeleteWithdraw(long withdrawID)
        {
            validateID(withdrawID);

            var _fWithdraw = FindByID(withdrawID);
            if (_fWithdraw != null)
            {
                _db.Withdraws.Remove(_fWithdraw);
                _db.SaveChanges();
            }
        }

        public WithdrawModel? FindByID(long withdrawID)
        {
            var _fWithdraw = _db.Withdraws.Where(x => x.WithdrawID == withdrawID).FirstOrDefault();
            if (_fWithdraw == null)
            {
                throw new Exception("withdraw_not_found");
            }
            return _fWithdraw;
        }

        public WithdrawModel? FindWithdrawByID(long withdrawID)
        {
            return FindByID(withdrawID);
        }

        public List<WithdrawModel> GetWithdraws()
        {
            return _db.Withdraws.ToList();
        }
    }
}
