﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Airline.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Airline.Services
{
    public class PesawatService : BaseService
    {
        public PesawatService(AirlineBookingDbContext db) : base(db)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE pesawat;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }


        private void validateBase(string kodePenerbangan,
            string kodeJenisPesawat,
            string kodeMaskapai)
        {
            if (string.IsNullOrEmpty(kodePenerbangan))
            {
                throw new Exception("kode_penerbangan_kosong");
            }
            if (string.IsNullOrEmpty(kodeJenisPesawat))
            {
                throw new Exception("kode_jenis_pesawat");
            }
            if (string.IsNullOrEmpty(kodeMaskapai))
            {
                throw new Exception("kode_maskapai");
            }
        }

        private void setValue(ref Models.PesawatModel _pesawat,
            string kodePenerbangan,
            string kodeJenisPesawat,
            string kodeMaskapai)
        {

            _pesawat.KodePenerbangan = kodePenerbangan;
            _pesawat.KodeJenisPesawat = kodeJenisPesawat;
            _pesawat.KodeMaskapai = kodeMaskapai;

        }

        public void AddPesawat(string kodePenerbangan,
            string kodeJenisPesawat,
            string kodeMaskapai)
        {

            validateBase(kodePenerbangan, kodeJenisPesawat,
                kodeMaskapai);

            var _pesawat = new Models.PesawatModel();

            setValue(ref _pesawat, kodePenerbangan, kodeJenisPesawat,
                kodeMaskapai);

            _db.Pesawats.Add(_pesawat);

            _db.SaveChanges();

        }

        private Models.PesawatModel findByKodePenerbangan(string kodePenerbangan)
        {
            if (string.IsNullOrEmpty(kodePenerbangan))
            {
                throw new ArgumentNullException("kode_penerbangan_is_required");
            }

            var _fPesawat = _db.Pesawats?.Where(x => x.KodePenerbangan.ToLower().Equals(kodePenerbangan)).FirstOrDefault();
            if (_fPesawat == null)
            {
                throw new NullReferenceException("pesawat_not_found");
            }
            return _fPesawat;
        }


        public Models.PesawatModel FindByKodePenerbangan(string kodePenerbangan)
        {
            return findByKodePenerbangan(kodePenerbangan);
        }

    }
}
