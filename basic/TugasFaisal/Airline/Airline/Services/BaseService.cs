﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Airline.Models;
using System.Threading.Tasks;

namespace Airline.Services
{
    public class BaseService
    {
        protected Models.AirlineBookingDbContext _db;
        public BaseService(Models.AirlineBookingDbContext db)
        {
            _db = db;
        }
    }
}
