﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airline.Models;
using Microsoft.EntityFrameworkCore;

namespace Airline.Services
{
    public class AirportService : BaseService
    {
        public AirportService(AirlineBookingDbContext db) : base(db)
        {
        }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE airport;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();
            _dbConn.Close();
        }


        private void validateBase(string kodeAirport,
            string namaAirport,
            string propinsi,
            string kota)
        {
            if (string.IsNullOrEmpty(kodeAirport))
            {
                throw new Exception("kode_airport_kosong");
            }
            if (string.IsNullOrEmpty(namaAirport))
            {
                throw new Exception("nama_airport_kosong");
            }
            if (string.IsNullOrEmpty(propinsi))
            {
                throw new Exception("propinsi_kosong");
            }
            if (string.IsNullOrEmpty(kota))
            {
                throw new Exception("kota_kosong");
            }
        }

        private void setValue(ref Models.AirportModel _airport,
            string kodeAirport,
            string namaAirport,
            string propinsi,
            string kota)
        {
            _airport.KodeAiport = kodeAirport;
            _airport.NamaAirport = namaAirport;
            _airport.Propinsi = propinsi;
            _airport.Kota = kota;
        }

        public void AddAirport(string kodeAirport,
            string namaAirport,
            string propinsi,
            string kota)
        {
            validateBase(kodeAirport, namaAirport, propinsi, kota);

            var _airport = new Models.AirportModel();

            setValue(ref _airport, kodeAirport, namaAirport, propinsi, kota);
            _db.Airports.Add(_airport);
            _db.SaveChanges();

        }

        private Models.AirportModel findByKodeAirport(string kodeAirport)
        {
            if (string.IsNullOrEmpty(kodeAirport))
            {
                throw new ArgumentNullException("kode_airport_is_null");
            }
            var _fAirport = _db.Airports?.Where(x => x.KodeAiport.ToLower().Equals(kodeAirport)).FirstOrDefault();
            if (_fAirport == null)
            {
                throw new NullReferenceException("airport_not_found");
            }
            return _fAirport;
        }

        public Models.AirportModel FindAirportByKodeAirport(string kodeAirport)
        {
            return findByKodeAirport(kodeAirport);
        }

    }
}

