﻿using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace Airline.Services
{
    public class BookingService : BaseService
    {
        public BookingService(Models.AirlineBookingDbContext db) : base(db) { }

        public void Migrate()
        {
            var _dbConn = _db.Database.GetDbConnection();
            _dbConn.Open();
            var _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE booking;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();

            _cmd = _dbConn.CreateCommand();
            _cmd.CommandText = "TRUNCATE TABLE booking_penumpang;";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.ExecuteNonQuery();

            _dbConn.Close();
        }

        private string createKodeBooking()
        {
            return string.Format("%x", Helpers.IDHelper.ID());
        }
        public long NewBooking(long scheduleID)
        {
            try
            {
                var _schedule = _db.Schedules
                    .Include(x => x.AirportAsal)
                    .Include(x => x.AirportTujuan)
                    .FirstOrDefault(s => s.ScheduleID == scheduleID);

                if (_schedule == null)
                {
                    throw new Exception("schedule_not_found");
                }

                var _booking = new Models.BookingModel();
                _booking.KodeBooking = createKodeBooking();
                _booking.KodeAirportAsal = _schedule.KodeAirportAsal;


            }
            catch (Exception ex)
            {

            }

            return 0;

        }
    }

}