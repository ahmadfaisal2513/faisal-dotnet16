﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Airline.Models
{
    [Table("pesawat")]
    public class PesawatModel
    {

        [Key]
        [Column("kode_penerbangan", TypeName = "varchar(30)")]
        public string KodePenerbangan { get; set; } //PK

        [Column("kode_jenis_pesawat", TypeName = "varchar(30)")]
        public string KodeJenisPesawat { get; set; } //FK

        [Column("kode_maskapai", TypeName = "varchar(30)")]
        public string KodeMaskapai { get; set; }


    }
}
