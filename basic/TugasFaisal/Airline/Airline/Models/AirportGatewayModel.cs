﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Airline.Models
{
    [Table("airport_gateway")]
    public class AirportGatewayModel
    {
        [Key]
        [Column("id_gateway", TypeName = "bigint")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long IDGateway { get; set; } //PK

        [Column("kode_airport", TypeName = "varchar(30)")]
        public string KodeAirport { get; set; } //FK

        [Column("nomor_gate", TypeName = "varchar(5)")]
        public string NomorGate { get; set; }

        [Column("nomor_pintu", TypeName = "varchar(5)")]
        public string NomorPintu { get; set; }


        [Column("created_date", TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }


        [Column("updated_date", TypeName = "datetime")]
        public DateTime? UpdateDate { get; set; }


        public AirportGatewayModel() { }

        public AirportGatewayModel(string kodeAirport,
            string nomorGate,
            string nomorPintu)
        {
            this.KodeAirport = kodeAirport;
            this.NomorGate = nomorGate;
            this.NomorPintu = nomorPintu;
        }

    }

}