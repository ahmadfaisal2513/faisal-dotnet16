﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Airline.Models
{
    [Table("schedule")]
    public class ScheduleModel
    {

        [Key]
        [Column("schedule_id", TypeName = "bigint")]
        public long ScheduleID { get; set; }

        [Column("kode_maskapai", TypeName = "varchar(30)")]
        public string KodeMaskapai { get; set; }

        [Column("kode_penerbangan", TypeName = "varchar(30)")]
        public string KodePenerbangan { get; set; }

        [Column("kode_asal_airport", TypeName = "varchar(30)")]
        public string KodeAirportAsal { get; set; }

        [Column("kode_asal_tujuan", TypeName = "varchar(30)")]
        public string KodeAirportTujuan { get; set; }

        [Column("tgl_keberangkatan", TypeName = "datetime")]
        public DateTime TglKeberangkatan { get; set; }

        [NotMapped]
        public DateTime JamKeberangkatan { get; set; }


        [Column("tgl_kedatangan", TypeName = "datetime")]
        public DateTime TglKedatangan { get; set; }


        [NotMapped]
        public DateTime JamKedatangan { get; set; }

        [Column("batas_bagasi", TypeName = "int")]
        public int BatasBagasi { get; set; }

        [Column("batas_bagasi_kabin", TypeName = "int")]
        public int BatasBagasiKabin { get; set; }


        [NotMapped]
        /*[ForeignKey("KodeAirportAsal")]*/
        public AirportModel AirportAsal { get; set; }

        [NotMapped]
        /*[ForeignKey("KodeAirportTujuan")]*/
        public AirportModel AirportTujuan { get; set; }



    }
}
