﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Airline.Models;
using Airline.Services;
using System.Collections.Generic;
using Airline.Test;

namespace AirlineBooking.Test
{
    public class BookingServiceTest
    {
        public void TestTambahBooking_PositiveTest()
        {
            using (var _db = new AirlineBookingDbContext(TestVars.ConnectionString))
            {
                BookingService _bookingService = new BookingService(_db);
                _bookingService.Migrate();

                string _idAirportAsal = "B001";
                string _idAirportTujuan = "A003";
                string _namaAirportAsal = "CGK";
                string _namaAirportTujuan = "DPS";
                string _tglKeberangkatan = "2022-02-26";
                string _jamKeberangkatan = "08:00 AM";
                string _tglKedatangan = "2022-02-26";
                string _jamKedatangan = "10:00 AM";
                int _durasi = 2;
                string _kodeMaskapai = "LR/LNI";
                string _namaMaskapai = "Lion Air";
                string _kodePenerbangan = "LR001";
                int _batasBagasi = 20;
                int _batasBagasiKabin = 7;
                string _metodeBayar = "VISA";
                decimal _hargaTiket = 1000000;
                int _jumlahPenumpang = 1;
                decimal _total = 1000000;
                string _kodePembayaran = "VS02103";
                string _kodeCheckin = "CK0291";
                int _statusBooking = 1;

                var _kodeBooking = _bookingService.TambahBooking(_idAirportAsal,
                    _namaAirportAsal,
                    _idAirportTujuan,
                    _namaAirportTujuan,
                    _tglKeberangkatan,
                    _jamKeberangkatan,
                    _tglKedatangan,
                    _jamKedatangan,
                    _durasi,
                    _kodeMaskapai,
                    _namaMaskapai,
                    _kodePenerbangan,
                    _batasBagasi,
                    _batasBagasiKabin,
                    _metodeBayar,
                    _hargaTiket,
                    _jumlahPenumpang,
                    _total,
                    _kodePembayaran,
                    _kodeCheckin,
                    _statusBooking);

                BookingModel? _fBooking = _bookingService.FindScheduleByID(_kodeBooking);
                Assert.NotNull(_fBooking);

                if (_fBooking != null)
                {
                    Assert.Equal(_idAirportAsal, _fBooking.IDAirportAsal);
                    Assert.Equal(_namaAirportAsal, _fBooking.NamaAirportAsal);
                    Assert.Equal(_idAirportTujuan, _fBooking.IDAiportTujuan);
                    Assert.Equal(_namaAirportTujuan, _fBooking.NamaAirportTujuan);

                    Assert.Equal(_tglKeberangkatan, _fBooking.TglKeberangkatan.ToString("yyyy-MM-dd"));
                    Assert.Equal(_jamKeberangkatan, _fBooking.JamKeberangkatan.ToString("hh:mm tt"));

                    Assert.Equal(_tglKedatangan, _fBooking.TglKedatangan.ToString("yyyy-MM-dd"));
                    Assert.Equal(_jamKedatangan, _fBooking.JamKedatangan.ToString("hh:mm tt"));

                    Assert.Equal(_durasi, _fBooking.Durasi);
                    Assert.Equal(_kodeMaskapai, _fBooking.KodeMaskapai);
                    Assert.Equal(_namaMaskapai, _fBooking.NamaMaskapai);
                    Assert.Equal(_kodePenerbangan, _fBooking.KodePenerbangan);
                    Assert.Equal(_batasBagasi, _fBooking.BatasBagasi);
                    Assert.Equal(_batasBagasiKabin, _fBooking.BatasBagasiKabin);
                    Assert.Equal(_metodeBayar, _fBooking.MetodeBayar);
                    Assert.Equal(_hargaTiket, _fBooking.HargaTiket);
                    Assert.Equal(_jumlahPenumpang, _fBooking.JumlahPenumpang);
                    Assert.Equal(_total, _fBooking.Total);
                    Assert.Equal(_kodePembayaran, _fBooking.KodePembayaran);
                    Assert.Equal(_kodeCheckin, _fBooking.KodeCheckIn);
                    Assert.Equal(_statusBooking, _fBooking.StatusBooking);
                }


                List<BookingModel> _Bookings = _bookingService.GetBookings();
                Assert.Equal(1, _Bookings.Count);
            }
        }
    }
}