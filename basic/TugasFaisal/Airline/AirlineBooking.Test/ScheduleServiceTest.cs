using Xunit;
using Airline.Models;
using Airline.Services;
using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;

namespace Airline.Test
{
    /*
    * developer new project
    * developer maintenance project
    * QA Developer / Test Developer
    * Lead Technical 
    */
    public class ScheduleServiceTest
    {
        /*
         * Negative Test / False Test
         * 
         */
        [Fact]
        public void TestSchedule_NegativeTest()
        {

            ScheduleService _scheduleService = new ScheduleService(null);

            string _kodeMaskapai = "";
            string _kodePenerbangan = "";
            string _kodeAirportAsal = "";
            string _kodeAirportTujuan = "";
            string _tglKeberangkatan = "";
            string _jamKeberangkatan = "";
            string _tglKedatangan = "";
            string _jamKedatangan = "";
            int _batasBagasi = -1;
            int _batasBagasiKabin = -1;


            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                    _kodePenerbangan,
                    _kodeAirportAsal,
                    _kodeAirportTujuan,
                    _tglKeberangkatan,
                    _jamKeberangkatan,
                    _tglKedatangan,
                    _jamKedatangan,
                    _batasBagasi,
                    _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Equal("kode_maskapai_kosong", ex.Message);
            }


            _kodeMaskapai = "LR/LNI";
            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                   _kodePenerbangan,
                   _kodeAirportAsal,
                   _kodeAirportTujuan,
                   _tglKeberangkatan,
                   _jamKeberangkatan,
                   _tglKedatangan,
                   _jamKedatangan,
                   _batasBagasi,
                   _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Equal("kode_penerbangan_kosong", ex.Message);
            }



            _kodePenerbangan = "JT619";
            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                   _kodePenerbangan,
                   _kodeAirportAsal,
                   _kodeAirportTujuan,
                   _tglKeberangkatan,
                   _jamKeberangkatan,
                   _tglKedatangan,
                   _jamKedatangan,
                   _batasBagasi,
                   _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Equal("kode_airport_asal_kosong", ex.Message);
            }




            _kodeAirportAsal = "CGK";
            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                   _kodePenerbangan,
                   _kodeAirportAsal,
                   _kodeAirportTujuan,
                   _tglKeberangkatan,
                   _jamKeberangkatan,
                   _tglKedatangan,
                   _jamKedatangan,
                   _batasBagasi,
                   _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Equal("kode_airport_tujuan_kosong", ex.Message);
            }




            _kodeAirportTujuan = "DPS";
            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                   _kodePenerbangan,
                   _kodeAirportAsal,
                   _kodeAirportTujuan,
                   _tglKeberangkatan,
                   _jamKeberangkatan,
                   _tglKedatangan,
                   _jamKedatangan,
                   _batasBagasi,
                   _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Equal("tgl_keberangkatan_kosong", ex.Message);
            }



            _tglKeberangkatan = "2022-02-24";

            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                   _kodePenerbangan,
                   _kodeAirportAsal,
                   _kodeAirportTujuan,
                   _tglKeberangkatan,
                   _jamKeberangkatan,
                   _tglKedatangan,
                   _jamKedatangan,
                   _batasBagasi,
                   _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Equal("jam_keberangkatan_kosong", ex.Message);
            }



            _jamKeberangkatan = "08:00";

            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                   _kodePenerbangan,
                   _kodeAirportAsal,
                   _kodeAirportTujuan,
                   _tglKeberangkatan,
                   _jamKeberangkatan,
                   _tglKedatangan,
                   _jamKedatangan,
                   _batasBagasi,
                   _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Equal("tgl_kedatangan_kosong", ex.Message);
            }



            _tglKedatangan = "2022-02-24";
            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                   _kodePenerbangan,
                   _kodeAirportAsal,
                   _kodeAirportTujuan,
                   _tglKeberangkatan,
                   _jamKeberangkatan,
                   _tglKedatangan,
                   _jamKedatangan,
                   _batasBagasi,
                   _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Equal("jam_kedatangan_kosong", ex.Message);
            }



            _jamKedatangan = "10:00";
            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                   _kodePenerbangan,
                   _kodeAirportAsal,
                   _kodeAirportTujuan,
                   _tglKeberangkatan,
                   _jamKeberangkatan,
                   _tglKedatangan,
                   _jamKedatangan,
                   _batasBagasi,
                   _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Equal("batas_bagasi_harus_lebih_besar_atau_sama_dengan_nol", ex.Message);
            }



            _batasBagasi = 20;
            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                   _kodePenerbangan,
                   _kodeAirportAsal,
                   _kodeAirportTujuan,
                   _tglKeberangkatan,
                   _jamKeberangkatan,
                   _tglKedatangan,
                   _jamKedatangan,
                   _batasBagasi,
                   _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Equal("batas_bagasi_kabin_harus_lebih_besar_atau_sama_dengan_nol", ex.Message);
            }


            _batasBagasiKabin = 7;
            _tglKeberangkatan = "2022-02-3";
            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                  _kodePenerbangan,
                  _kodeAirportAsal,
                  _kodeAirportTujuan,
                  _tglKeberangkatan,
                  _jamKeberangkatan,
                  _tglKedatangan,
                  _jamKedatangan,
                  _batasBagasi,
                  _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                /*String '2022-02-3' was not recognized as a valid DateTime.*/
                Assert.Contains("was not recognized as a valid DateTime", ex.Message);
            }

            _jamKeberangkatan = "25:00";
            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                  _kodePenerbangan,
                  _kodeAirportAsal,
                  _kodeAirportTujuan,
                  _tglKeberangkatan,
                  _jamKeberangkatan,
                  _tglKedatangan,
                  _jamKedatangan,
                  _batasBagasi,
                  _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Contains("was not recognized as a valid DateTime", ex.Message);
            }

            _tglKedatangan = "2022-13-01";
            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                  _kodePenerbangan,
                  _kodeAirportAsal,
                  _kodeAirportTujuan,
                  _tglKeberangkatan,
                  _jamKeberangkatan,
                  _tglKedatangan,
                  _jamKedatangan,
                  _batasBagasi,
                  _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Contains("was not recognized as a valid DateTime", ex.Message);
            }

            _jamKedatangan = ":00";
            try
            {
                _scheduleService.TambahSchedule(_kodeMaskapai,
                  _kodePenerbangan,
                  _kodeAirportAsal,
                  _kodeAirportTujuan,
                  _tglKeberangkatan,
                  _jamKeberangkatan,
                  _tglKedatangan,
                  _jamKedatangan,
                  _batasBagasi,
                  _batasBagasiKabin);
            }
            catch (Exception ex)
            {
                Assert.Contains("was not recognized as a valid DateTime", ex.Message);
            }


        }


        /*Positive Test */

        [Fact]
        public void TestTambahSchedule_PositiveTest()
        {

            var dbOption = new DbContextOptionsBuilder<AirlineBookingDbContext>()
                .UseSqlServer(TestVars.ConnectionString)
                .Options;

            using (var _db = new Models.AirlineBookingDbContext(dbOption))
            {

                ScheduleService _scheduleService = new ScheduleService(_db);
                _scheduleService.Migrate();

                string _kodeMaskapai = "LR/LNI";
                string _kodePenerbangan = "JT619";
                string _kodeAirportAsal = "CGK";
                string _kodeAirportTujuan = "DPS";
                string _tglKeberangkatan = "2022-02-24";
                string _jamKeberangkatan = "08:00 AM";
                string _tglKedatangan = "2022-02-24";
                string _jamKedatangan = "10:00 AM";
                int _batasBagasi = 20;
                int _batasBagasiKabin = 7;


                var _scheduleID = _scheduleService.TambahSchedule(_kodeMaskapai,
                    _kodePenerbangan,
                    _kodeAirportAsal,
                    _kodeAirportTujuan,
                    _tglKeberangkatan,
                    _jamKeberangkatan,
                    _tglKedatangan,
                    _jamKedatangan,
                    _batasBagasi,
                    _batasBagasiKabin);



                Models.ScheduleModel? _fSchedule = _scheduleService.FindScheduleByID(_scheduleID);
                Assert.NotNull(_fSchedule);

                if (_fSchedule != null)
                {
                    Assert.Equal(_kodeMaskapai, _fSchedule.KodeMaskapai);
                    Assert.Equal(_kodePenerbangan, _fSchedule.KodePenerbangan);
                    Assert.Equal(_kodeAirportAsal, _fSchedule.KodeAirportAsal);
                    Assert.Equal(_kodeAirportTujuan, _fSchedule.KodeAirportTujuan);

                    Assert.Equal(_tglKeberangkatan, _fSchedule.TglKeberangkatan.ToString("yyyy-MM-dd"));
                    Assert.Equal(_jamKeberangkatan, _fSchedule.JamKeberangkatan.ToString("hh:mm tt"));

                    Assert.Equal(_tglKedatangan, _fSchedule.TglKedatangan.ToString("yyyy-MM-dd"));
                    Assert.Equal(_jamKedatangan, _fSchedule.JamKedatangan.ToString("hh:mm tt"));

                    Assert.Equal(_batasBagasi, _fSchedule.BatasBagasi);
                    Assert.Equal(_batasBagasiKabin, _fSchedule.BatasBagasiKabin);
                }


                List<ScheduleModel> _schedules = _scheduleService.GetSchedules();
                Assert.Equal(1, _schedules.Count);



                _jamKeberangkatan = "10:00 AM";
                _jamKedatangan = "12:00 PM";

                _scheduleService.UpdateSchedule(_scheduleID,
                    _kodeMaskapai,
                    _kodePenerbangan,
                    _kodeAirportAsal,
                    _kodeAirportTujuan,
                    _tglKeberangkatan,
                    _jamKeberangkatan,
                    _tglKedatangan,
                    _jamKedatangan,
                    _batasBagasi,
                    _batasBagasiKabin);

                _fSchedule = _scheduleService.FindScheduleByID(_scheduleID);
                Assert.NotNull(_fSchedule);
                if (_fSchedule != null)
                {
                    Assert.Equal(_kodeMaskapai, _fSchedule.KodeMaskapai);
                    Assert.Equal(_kodePenerbangan, _fSchedule.KodePenerbangan);
                    Assert.Equal(_kodeAirportAsal, _fSchedule.KodeAirportAsal);
                    Assert.Equal(_kodeAirportTujuan, _fSchedule.KodeAirportTujuan);

                    Assert.Equal(_tglKeberangkatan, _fSchedule.TglKeberangkatan.ToString("yyyy-MM-dd"));
                    Assert.Equal(_jamKeberangkatan, _fSchedule.JamKeberangkatan.ToString("hh:mm tt"));

                    Assert.Equal(_tglKedatangan, _fSchedule.TglKedatangan.ToString("yyyy-MM-dd"));
                    Assert.Equal(_jamKedatangan, _fSchedule.JamKedatangan.ToString("hh:mm tt"));

                    Assert.Equal(_batasBagasi, _fSchedule.BatasBagasi);
                    Assert.Equal(_batasBagasiKabin, _fSchedule.BatasBagasiKabin);
                }


                /*const string SCHEDULE_NOT_FOUND = "schedule_not_found";
                _scheduleService.DeleteSchedule(_scheduleID);
                try
                {
                    _fSchedule = _scheduleService.FindScheduleByID(_scheduleID);
                }
                catch (Exception e)
                {
                    Assert.Equal(SCHEDULE_NOT_FOUND, e.Message);
                }*/


                _kodeMaskapai = "ID/BTK";
                _kodePenerbangan = "BTK123";
                _kodeAirportAsal = "CGK";
                _kodeAirportTujuan = "DPS";
                _tglKeberangkatan = "2022-02-25";
                _jamKeberangkatan = "09:00 AM";
                _tglKedatangan = "2022-02-25";
                _jamKedatangan = "10:30 AM";
                _batasBagasi = 20;
                _batasBagasiKabin = 7;


                _scheduleID = _scheduleService.TambahSchedule(_kodeMaskapai,
                    _kodePenerbangan,
                    _kodeAirportAsal,
                    _kodeAirportTujuan,
                    _tglKeberangkatan,
                    _jamKeberangkatan,
                    _tglKedatangan,
                    _jamKedatangan,
                    _batasBagasi,
                    _batasBagasiKabin);

                _fSchedule = _scheduleService.FindScheduleByID(_scheduleID);
                Assert.NotNull(_fSchedule);

                if (_fSchedule != null)
                {
                    Assert.Equal(_kodeMaskapai, _fSchedule.KodeMaskapai);
                    Assert.Equal(_kodePenerbangan, _fSchedule.KodePenerbangan);
                    Assert.Equal(_kodeAirportAsal, _fSchedule.KodeAirportAsal);
                    Assert.Equal(_kodeAirportTujuan, _fSchedule.KodeAirportTujuan);

                    Assert.Equal(_tglKeberangkatan, _fSchedule.TglKeberangkatan.ToString("yyyy-MM-dd"));
                    Assert.Equal(_jamKeberangkatan, _fSchedule.JamKeberangkatan.ToString("hh:mm tt"));

                    Assert.Equal(_tglKedatangan, _fSchedule.TglKedatangan.ToString("yyyy-MM-dd"));
                    Assert.Equal(_jamKedatangan, _fSchedule.JamKedatangan.ToString("hh:mm tt"));

                    Assert.Equal(_batasBagasi, _fSchedule.BatasBagasi);
                    Assert.Equal(_batasBagasiKabin, _fSchedule.BatasBagasiKabin);
                }

                _schedules = _scheduleService.GetSchedules();
                Assert.Equal(2, _schedules.Count);


            }

        }

        seeding data
    }
}