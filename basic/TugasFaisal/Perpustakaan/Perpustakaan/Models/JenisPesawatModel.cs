﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perpustakaan.Models
{
    public class JenisPesawatModel
    {
        public string IDJenisPesawat { get; set; }
        public string KodeJenisPesawat { get; set; }
        public string TahunPesawat { get; set; }

    }
}
