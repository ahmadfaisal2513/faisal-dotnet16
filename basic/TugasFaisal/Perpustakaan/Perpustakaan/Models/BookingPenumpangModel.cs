﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perpustakaan.Models
{
    public class BookingPenumpangModel
    {
        public string KTP { get; set; }
        public string KodeBooking { get; set; }
        public string Title { get; set; }
        public string NamaPenumpang { get; set; }
        public string NomorKursi { get; set; }

    }
}
