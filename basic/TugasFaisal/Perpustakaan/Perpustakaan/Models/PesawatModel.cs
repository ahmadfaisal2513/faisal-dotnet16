﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perpustakaan.Models
{
    public class PesawatModel
    {
        public string KodePenerbangan { get; set; } //PK

        public string IDJenisPesawat { get; set; } //FK

        public string KodeMaskapai { get; set; }
    }
}
