﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perpustakaan.Models
{
    public class KursiPesawatModel
    {
        public string IDKursiPesawat { get; set; } //PK
        public string KodePenerbangan { get; set; } //FK
        public string NomorKursi { get; set; }

    }
}
