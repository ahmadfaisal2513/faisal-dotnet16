﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perpustakaan.Models
{
    public class BookingModel
    {
        public string KodeBooking { get; set; }    //PK

        public string IDAirportAsal { get; set; }
        public string NamaAirportAsal { get; set; }
        public string IDAiportTujuan { get; set; }
        public string NamaAirportTujuan { get; set; }

        public DateTime TglKeberangkatan { get; set; }
        public DateTime JamKeberangkatan { get; set; }
        public DateTime TglKedatangan { get; set; }
        public DateTime JamKedatangan { get; set; }
        public int Durasi { get; set; }

        public string KodeMaskapai { get; set; }
        public string NamaMaskapai { get; set; }
        public string KodePenerbangan { get; set; }

        public int BatasBagasi { get; set; }
        public int BatasBagasiKabin { get; set; }

        public int MetodeBayar { get; set; }
        public decimal HargaTiket { get; set; }
        public int JumlahPenumpang { get; set; }
        public decimal Total { get; set; }
        public string KodePembayaran { get; set; }
        public string KodeCheckIn { get; set; }


        public int StatusBooking { get; set; }

    }
}
