﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perpustakaan.Models
{
    public class MaskapaiModel
    {
        public string KodeMaskapai { get; set; }
        public string NamaMaskapai { get; set; }

    }
}
