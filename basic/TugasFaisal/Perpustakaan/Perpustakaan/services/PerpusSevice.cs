﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perpustakaan.services
{
    public class PerpusSevice
    {
        private List<model.Book> _books = new List<model.Book>();

        public void addBook(int Rack, string BookName, List<model.Page> Pages)
        {
            var _book = new model.Book();

                _book.BookName = BookName;
                _book.Rack = Rack;
                _book.Pages = Pages;

                _books.Add(_book);
        }

    }
}
