﻿using System;

namespace tugas
{
    class Tugas
    {
        static void Main(string[] args)
        {
            Console.WriteLine("============================================================================================");
            Console.WriteLine("Daftar Karyawan");
            Console.WriteLine("============================================================================================");
            Console.WriteLine("Kode Karyawan  |  Nama Karyawan  |   Mulai Bekerja  |   Masih Bekerja |  Tgl Keluar");
            Console.WriteLine("============================================================================================");
            Console.WriteLine("E001           |  Faisal         |   10-Feb-2022    |   Masih         |");
            Console.WriteLine("E002           |  Vido           |   11-Feb-2022    |   Masih         |");
            Console.WriteLine("E003           |  Dinda          |   12-Feb-2022    |   Masih         |");
            Console.WriteLine("E004           |  Dwi Putri      |   12-Feb-2022    |   Masih         |");
            Console.WriteLine("E005           |  Susanto        |   12-Feb-2022    |   Masih         |");
            Console.WriteLine("E006           |  Fathan         |   12-Feb-2022    |   Masih         |");
            Console.WriteLine("....");
            Console.WriteLine("....");
            Console.WriteLine("E011           |  Riska          |   12-Feb-2022     |   Masih        |  31-Des-2022");
        }
    }
}
