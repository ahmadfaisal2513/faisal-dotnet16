﻿using System;

namespace project1
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("========================================================================================");
            Console.WriteLine("Account Balance Histories");
            Console.WriteLine("========================================================================================");
            Console.Write("Transaction Date\t"); Console.Write("|"); Console.Write("Customer Code\t"); Console.Write("|"); Console.Write("Customer Name\t"); Console.Write("|"); Console.Write("Transaction Amount\n");
            Console.WriteLine("========================================================================================");
            DateTime[] transactionDates = new DateTime[6]
            {
                DateTime.Parse("2022-02-11"),
                DateTime.Parse("2022-02-11"),
                DateTime.Parse("2022-02-12"),
                DateTime.Parse("2022-02-12"),
                DateTime.Parse("2022-02-12"),
                DateTime.Parse("2022-02-13"),
            };
            string[] customerCodes = new string[6]
            {
                "C001",
                "C002",
                "C002",
                "C003",
                "C001",
                "C003",
            };
            string[] customerNames = new string[6]
            {
                "Faisal",
                "Vido",
                "Vido",
                "Susanto",
                "Faisal",
                "Susanto",
            };
            decimal[] transactionAmounts = new decimal[6]
            {
                1000000,
                2000000,
                -500000,
                3000000,
                -500000,
                -500000,
            };
            decimal totaltransactionAmounts = 0;
            for (int i = 0; i < customerCodes.Length; i++)
            {
                Console.WriteLine("{0}\t\t|{1}\t\t|{2}\t\t|{3}\t\t\t|",
                    transactionDates[i].ToString("yyyy-MM-dd"),
                    customerCodes[i],
                    customerNames[i],
                    transactionAmounts[i]);
            }
            Console.WriteLine("========================================================================================");
            Console.WriteLine("Account Balance Summary");
            Console.WriteLine("========================================================================================");
            Console.Write("Customer Code\t"); Console.Write("|"); Console.Write("Customer Name\t"); Console.Write("|"); Console.Write("Transaction Amount\n");
            Console.WriteLine("========================================================================================");
            decimal C001 = transactionAmounts[0] + transactionAmounts[4];
            decimal C002 = transactionAmounts[1] + transactionAmounts[2];
            decimal C003 = transactionAmounts[3] + transactionAmounts[5];
            Console.Write(customerCodes[0]); Console.Write("\t\t"); Console.Write("|");
            Console.Write(customerNames[0]); Console.Write("\t\t"); Console.Write("|");
            Console.WriteLine(C001);
            Console.Write(customerCodes[1]); Console.Write("\t\t"); Console.Write("|");
            Console.Write(customerNames[1]); Console.Write("\t\t"); Console.Write("|");
            Console.WriteLine(C002);
            Console.Write(customerCodes[3]); Console.Write("\t\t"); Console.Write("|");
            Console.Write(customerNames[3]); Console.Write("\t\t"); Console.Write("|");
            Console.WriteLine(C003);
        }
    }
}
